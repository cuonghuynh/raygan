<?php
/*
Plugin Name: Munich Portfolio
Description: A portfolio plugin for the Explora Munich WordPress Themes
Version: 1.1
Author: Explora Themes
Author URI: http://www.explora.com.uy/
*/

// Load the global metaboxes helpers
if( file_exists( get_template_directory().'/admin/metaboxes/global.php' ) ) {
    include_once( get_template_directory().'/admin/metaboxes/global.php' );    
}

class MunichPortfolio {
    public static $portfolio_page_metaboxes;
    public static $portfolio_page_options_fields;

    public function __construct() {

        // Create custom Post Type and Taxonomy
        add_action( 'init', array( $this, 'munich_create_posttype' ) );
        add_action( 'init', array( $this, 'munich_create_taxonomy' ) );

        // Add custom columns to the posts list
        add_filter('manage_portfolio_posts_columns', array( $this, 'munich_portfolio_columns_head') );  
        add_action('manage_portfolio_posts_custom_column', array( $this, 'munich_portfolio_columns_content') , 10, 2); 

        // Add image sizes
        $this->munich_portfolio_add_image_sizes();


        // Enqueue the backend's styles and scripts.
        add_action( "redux/page/theme_options/enqueue", array( $this, 'munich_portfolio_enqueue_admin_assets' ) );

    }

    // ****************************************
    // Creates and adds the Portfolio post type
    // ****************************************
    public function munich_create_posttype(){
        $portfolio_args = array(
            'label'             => __('Gallery', 'munich'),
            'singular_label'    => __('Gallery', 'munich'),
            'public'            => true,
            'show_ui'           => true,
            'capability_type'   => 'post',
            'hierarchical'      => false,
            'supports'          => array('title', 'editor', 'thumbnail'),
            'has_archive'       => true,
            'rewrite'           => array ( 'slug' => 'portfolio' ),
        );
        register_post_type('portfolio', $portfolio_args);
    }

    // ****************************************
    // Creates and adds the Work Type taxonomy
    // ****************************************
    public function munich_create_taxonomy(){
        register_taxonomy(
            'work_type',
            'portfolio',
            array(
                // Hierarchical taxonomy (like categories)
                'hierarchical' => true,
                // This array of options controls the labels displayed in the WordPress Admin UI
                'labels' => array(
                    'name' => __('Gallery Types', 'munich'),
                    'singular_name' => __('Gallery Type', 'munich'),
                    'search_items' =>  __('Search Gallery Types', 'munich'),
                    'all_items' => __('All Gallery Types', 'munich'),
                    'parent_item' => __('Parent Gallery Type', 'munich'),
                    'parent_item_colon' => __('Parent Gallery Type:', 'munich'),
                    'edit_item' => __('Edit Gallery Type', 'munich'),
                    'update_item' => __('Update Gallery Type', 'munich'),
                    'add_new_item' => __('Add New Gallery Type', 'munich'),
                    'new_item_name' => __('New Gallery Type Name', 'munich'),
                    'menu_name' => __('Gallery Types', 'munich'),
                    // Control the slugs used for this taxonomy
                    'rewrite' => array(
                        'slug' => 'work_type', // This controls the base slug that will display before each term
                        'with_front' => false,  // Don't display the category base before "/work_types/"
                        'hierarchical' => true  // This will allow URL's like "/work_types/design/web_design/"
                    )
                )
            )
        );
    }

    // ****************************************
    // Adds columns to the WordPress posts list 
    // ****************************************
    public function munich_portfolio_columns_head($defaults) {  
        $defaults['munich_portfolio_thumbnail'] = __('Gallery Thumbnail', 'munich');  
        return $defaults;  
    }

    public function munich_portfolio_columns_content($column_name, $post_ID) {  
        if ($column_name == 'munich_portfolio_thumbnail') {
            $thumb_src = wp_get_attachment_image_src( get_post_thumbnail_id($post_ID), 'thumbnail' );
            if(isset($thumb_src) && sizeof($thumb_src) > 0) {
                echo '<img class="munich-work-thumb" width="100px" height="100px" src="'.$thumb_src[0].'" />';
            }
        }
    }

    // ****************
    // Adds image sizes
    // ****************
    protected function munich_portfolio_add_image_sizes(){
        add_image_size( 'wide_thumb', 768, 540, true );
        add_image_size( 'portrait_thumb', 384, 540, true );
    }

    // **********************
    // Portfolio Page Options
    // **********************

    // Portfolio Page Metaboxes

    public static function get_munich_portfolio_page_metaboxes($prefix){
        $portfolio_sections[] = array(
            'title' => __('Gallery Categories', 'munich'),
            'icon' => 'el-icon-check',
            'fields' => self::get_munich_portfolio_page_work_categories_fields($prefix)
        );
        $portfolio_sections[] = array(
            'title' => __('Page Options', 'munich'),
            'desc' => __("Configure this portfolio page.", 'munich'),
            'icon' => 'el-icon-filter',
            'fields' => array_merge( array(
                array(
                    'id'       => $prefix . '_theme',
                    'type'     => 'radio',
                    'title'    => __('Page Theme', 'munich'), 
                    'subtitle' => __('Do you want the filtering sidebar to be visible on this portfolio page?', 'munich'), 
                    'options'  => array (
                        'light' => 'Light',
                        'dark' => 'Dark (Default)'
                    ),
                    'default'  => 'dark'
                )
            ), self::get_munich_portfolio_page_sidebar_fields($prefix))
        );

        // Return the meta boxes array
        return array(
            'id'            => 'munich-portfolio-options',
            'title'         => __( 'Options', 'munich' ),
            'post_types'    => array( 'page', 'demo_metaboxes' ),
            'page_template' => array('portfolio.php'),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low
            'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
            'sections'      => $portfolio_sections
        );
    }

    // Portfolio Page Fields

    public static function get_munich_portfolio_page_fields($prefix) {
        return array_merge(self::get_munich_portfolio_page_work_categories_fields($prefix), self::get_munich_portfolio_page_sidebar_fields($prefix));
    }

    public static function get_munich_portfolio_page_work_categories_fields($prefix){
        return array(  
            array(
                'id'        => $prefix . '_page_work_types',
                'type'      => 'checkbox',
                'title'     => __('Gallery Types', 'munich'),
                'subtitle' => __('Choose the gallery categories that you want to showcase on this portfolio page.<br><br><em>Leave all unchecked to show your full portfolio</em>.', 'munich'), 
                'data'     => 'terms',
                'args'     => array('taxonomies'=>'work_type', 'args'=>array()),
            )
        );
    }

    public static function get_icons_fields($folder_name = "", $path = "", $path_url = "") {
        if( strlen( $path ) == 0 ) $path = plugin_dir_path( __FILE__ ) . 'assets/images/';
        if( strlen( $path_url ) == 0 ) $path_url = plugin_dir_url( __FILE__ ) . 'assets/images/';
        if( strlen( $folder_name ) == 0 ) $folder_name = 'icons';
        
        $icons = scandir( $path . $folder_name );
        $icons_fields = array();
        if( is_array( $icons ) ) {
            foreach ($icons as $icon) {
                if( strpos($icon, ".png") ) {
                    $icon_name = str_replace(".png", "", $icon);
                    $icons_fields[$icon_name] = array(
                        'alt'   => $icon_name,
                        'img'   => $path_url . $folder_name . '/' . $icon
                    );
                }
            }
        }
        return $icons_fields;
    }

    public static function get_munich_portfolio_page_sidebar_fields($prefix){

        // The actual fields
        return array(  
            array(
                'id'       => $prefix . '_sidebar_visibility',
                'type'     => 'radio',
                'title'    => __('Sidebar Visibility', 'munich'), 
                'subtitle' => __('Do you want the filtering sidebar to be visible on this gallery page?', 'munich'), 
                'options'  => array (
                    'visible' => 'Visible',
                    'hidden' => 'Hidden'
                ),
                'default'  => 'visible'
            ),
            array(
                'id'       => $prefix . '_show_overlays',
                'type'     => 'radio',
                'title'    => __('Gallery Archive items overlay', 'munich'), 
                'subtitle' => __('Do you want to display the dark overlay on top of the gallery items?', 'munich'), 
                'options'  => array (
                    'visible' => 'Visible',
                    'hidden' => 'Hidden'
                ),
                'default'  => 'visible'
            ),
            array(
               'id' => 'section-start',
               'type' => 'section',
               'title' => __('Sidebar Header', 'munich'),
               'indent' => true 
            ),
            array(
                'id'       => $prefix . '_sidebar_header',
                'type'     => 'radio',
                'title'    => __('Sidebar Header Type', 'munich'), 
                'subtitle' => __('You can choose to show text on your sidebar\'s header, or an image.', 'munich'), 
                'options'  => array (
                    'text' => 'Text (with an optional icon)',
                    'image' => 'Image'
                ),
                'default'  => 'text'
            ),
            array(
                'id'       => $prefix . '_sidebar_header_image',
                'type'     => 'media',
                'title'    => __('Sidebar Header Image', 'munich'), 
                'subtitle' => __('Choose an image/logo to show on the right sidebar of your gallery page.', 'munich')
            ),
            array(
                'id'       => $prefix . '_sidebar_header_icon',
                'type'     => 'image_select',
                'title'    => __('Sidebar Header Icon', 'munich'), 
                'subtitle' => __('Icon to show on top of your sidebar text.', 'munich'),
                'options'  => self::get_icons_fields('sidebar_icons'),
                'default'  => 'icon-star'
            ),
            array(
                'id'       => $prefix . '_sidebar_header_title',
                'type'     => 'text',
                'title'    => __('Sidebar Header Title', 'munich'), 
                'default'  => 'LIST OF PROJECTS'
            ),
            array(
                'id'       => $prefix . '_sidebar_header_subtitle',
                'type'     => 'text',
                'title'    => __('Sidebar Header Subtitle', 'munich'), 
                'default'  => 'Hope you enjoy the view!'
            ),
            array(
                'id'     => 'section-end',
                'type'   => 'section',
                'indent' => false,
            )
        );
    }

    // *******************************
    // Portfolio Default Theme Options
    // *******************************

    public static function munich_portfolio_get_theme_options($prefix){
        return array(
            'icon'      => 'el-icon-puzzle',
            'title'     => __('Gallery', 'munich'),
            'fields'    => self::get_munich_portfolio_page_fields($prefix)
        );
    }

    // **********************
    // Portfolio Item Options
    // **********************

    // Portfolio Item Metaboxes

    public static function get_munich_portfolio_item_options_metabox(){

        $portfolio_sections = array();

        $portfolio_sections[] = array(
            'title' => __('General Options', 'munich'),
            'desc' => __("Choose the options for displaying this portfolio item.", 'munich'),
            'icon' => 'el-icon-view-mode',
            'fields' => array(  
                array(
                    'id'       => 'munich_portfolio_block_type',
                    'type'     => 'radio',
                    'title'    => __('Block type', 'munich'),
                    'subtitle'    => __('Choose the way you want this gallery to be displayed on your portfolio list page.', 'munich'),
                    'options'  => array (
                        'portrait' => 'Portrait Block',
                        'wide' => 'Wide Block'
                    ),
                    'default'  => 'wide'
                ),
            )
        );

        $portfolio_sections[] = array(
            'title' => __('Related Gallery', 'munich'),
            'desc' => __("Choose which related gallery to show on this gallery's page.<br><em>If you don't choose any gallery, the related gallery section will be hidden.</em>", 'munich'),
            'icon' => 'el-icon-star-empty',
            'fields' => array(  
                array(
                    'id'        => 'munich_portfolio_item_related_works',
                    'type'      => 'checkbox',
                    'title'     => __('Related Gallery', 'munich'),
                    'subtitle' => __("Choose which related works to show on this gallery's page", 'munich'), 
                    'data'     => 'posts',
                    'args'     => array('post_type'=>'portfolio', 'posts_per_page' => -1)
                )
            )
        );

        // Return the meta boxes array
        return array(
            'id'            => 'munich-portfolio-options',
            'title'         => __( 'Options', 'munich' ),
            'post_types'    => array( 'portfolio', 'demo_metaboxes' ),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low
            'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
            'sections'      => $portfolio_sections
        );
    }

    public static function get_munich_portfolio_item_slider_metabox(){

        $portfolio_sections = array();

        if( function_exists( 'add_gallery_metabox_fields' ) ) {
            $portfolio_sections = add_gallery_metabox_fields( $portfolio_sections, 'munich_portfolio_slider' );    
        }

        // Return the meta boxes array
        return array(
            'id'            => 'munich-portfolio-slider',
            'title'         => __( 'Slider', 'munich' ),
            'post_types'    => array( 'portfolio', 'demo_metaboxes' ),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low
            'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
            'sections'      => $portfolio_sections
        );
    }

    // *****************************************
    // Enqueues the backend's styles and scripts
    // *****************************************
    function munich_portfolio_enqueue_admin_assets() {
        wp_register_style( 'munich-portfolio-admin-custom-css', plugin_dir_url( __FILE__ ) . 'assets/admin-style.css', array( 'redux-css' ), time(), 'all' );
        wp_enqueue_style('munich-portfolio-admin-custom-css');
        wp_register_script( 'munich-portfolio-admin-custom-js', plugin_dir_url( __FILE__ ) . 'assets/admin.js', array( 'redux-js' ), time(), 'all' );
        wp_enqueue_script('munich-portfolio-admin-custom-js');
    }

}

$munich_portfolio = new MunichPortfolio();
