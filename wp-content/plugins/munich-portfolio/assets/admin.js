var MunichPortfolio;

(function( $ ) {

	MunichPortfolio = {

		options: {
			portfolioItemElements: {
				work_meta: {
					icons_fieldsets: $("[id^=theme_options-munich_portfolio_meta_icon_"),
					meta_fields: $("#munich_portfolio_meta-list > li > input"),
				}
			}
		},

		init: function(){
			this.metaIcons( this );
		},
		metaIcons: function(self){
			var icons_fieldsets = self.options.portfolioItemElements.work_meta.icons_fieldsets;
			var meta_fields = self.options.portfolioItemElements.work_meta.meta_fields;

			meta_fields.each(function(){
				var meta_field = $(this);
				var icon_fieldset = $(icons_fieldsets[$(this).parent().index()]);

				createIconToggleButton(meta_field, icon_fieldset);
			});

			function createIconToggleButton(meta_field, icon_fieldset){
				var selected_image = icon_fieldset.find('.redux-image-select-selected > img');

				meta_field.before('<i class="toggle-icon-button" style="background-image: url(' + selected_image.attr('src') + ')"></i>');
				
				// Move the icons fieldsets inside the meta fields
				var container_row = icon_fieldset.parent().parent();
				icon_fieldset.appendTo( meta_field.parent() );

				// Remove the remaining rows
				container_row.remove();

				// Show the icons fieldsets on click
				meta_field.parent().find('i.toggle-icon-button').on('click', function(){
					if( meta_field.parent().find('fieldset').hasClass('visible') ){
						meta_field.parent().parent().find('fieldset').removeClass('visible');
					}else{
						meta_field.parent().parent().find('fieldset').removeClass('visible');
						meta_field.parent().find('fieldset').addClass('visible');
					}
				});

				$('<i class="close">x</i>').appendTo(icon_fieldset);
				icon_fieldset.find('i.close').on('click', function(){
					icon_fieldset.removeClass('visible');
				});


				// Change the selected icon on click
				icon_fieldset.find('li.redux-image-select').on('click', function(){
					var selected_image = $(this).find('img');
					meta_field.parent().find('i.toggle-icon-button').css('background-image', 'url(' + selected_image.attr('src') + ')')
					icon_fieldset.removeClass('visible');
				});
			}
		}

	}

})(jQuery);

MunichPortfolio.init();