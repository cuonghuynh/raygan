<?php
/*
Plugin Name: Munich Blog
Description: A blog extension plugin for the Explora Munich WordPress Themes
Version: 1.0
Author: Explora Themes
Author URI: http://www.explora.com.uy/
*/

// Load the global metaboxes helpers
if( file_exists( get_template_directory().'/admin/metaboxes/global.php' ) ) {
    include_once( get_template_directory().'/admin/metaboxes/global.php' );    
}

class MunichBlog {

    public function __construct() {
        // Add custom columns to the posts list
        add_filter('excerpt_length', array( $this, 'munich_custom_excerpt_length'), 30 );  
        add_action('pre_get_posts', array( $this, 'munich_blog_posts_per_page') ); 
    }

    // Posts excerpt length
    public function munich_custom_excerpt_length( $length ) {
		return 20;
	}

	// Force posts per page to be unlimited
	public function munich_blog_posts_per_page( $query ) {
		if( $query->is_main_query() && !is_admin() ) {
			$query->set( 'posts_per_page', '-1' );
		}
	}

	// Single Post slider metabox
	public static function get_munich_blog_slider_metabox(){

        $blog_sections = array();

        if( function_exists( 'add_gallery_metabox_fields' ) ) {
            $blog_sections = add_gallery_metabox_fields( $blog_sections, 'munich_blog_slider' );    
        }

        // Return the meta boxes array
        return array(
            'id'            => 'munich-blog-slider',
            'title'         => __( 'Slider', 'fusion-framework' ),
            'post_types'    => array( 'post', 'demo_metaboxes' ),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low
            'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
            'sections'      => $blog_sections
        );
    }

}

$munich_blog = new MunichBlog();