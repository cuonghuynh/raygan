<?php
/**
 * Munich functions and definitions
 *
 * @package Munich
 */

/**
 * Add Redux Framework & extras
 */
 require get_template_directory() . '/admin/admin-init.php';

 /**
 * Require helper functions
 */
 require get_template_directory() . '/helpers/utils.php';
 require get_template_directory() . '/helpers/custom-styles-scripts.php';

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function munich_setup() {

	/*
	 * Make theme available for translation.
	 * Translations are located in the /languages/ directory.
	 */
	load_theme_textdomain( 'munich', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails on posts and pages.
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Main Menu', 'munich' )
	) );
	
	//Switch default core markup for search form, comment form, and comments to output valid HTML5.
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
    
    // Add WooCommeerce Support
    add_theme_support( 'woocommerce' );

}
add_action( 'after_setup_theme', 'munich_setup' );

/**
 * Register widget area.
 */
function munich_widgets_init() {
	// register_sidebar( array(
	// 	'name'          => __( 'Blog Sidebar', 'munich' ),
	// 	'id'            => 'blog-sidebar',
	// 	'description'   => '',
	// 	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	// 	'after_widget'  => '</aside>',
	// 	'before_title'  => '<h5 class="widget-title">',
	// 	'after_title'   => '</h5>',
	// ) );
	// register_sidebar( array(
	// 	'name'          => __( 'Shop Sidebar', 'munich' ),
	// 	'id'            => 'shop-sidebar',
	// 	'description'   => '',
	// 	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	// 	'after_widget'  => '</aside>',
	// 	'before_title'  => '<h5 class="widget-title">',
	// 	'after_title'   => '</h5>',
	// ) );
    register_sidebar( array(
		'name'          => __( 'Portfolio Sidebar', 'munich' ),
		'id'            => 'portfolio-sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h5 class="widget-title">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'munich_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function munich_enqueue_assets(){
    /* Munich Scripts */
	wp_enqueue_script('jquery');
	wp_enqueue_script('munich-helpers', get_template_directory_uri().'/assets/js/helpers.js', false, '1.0', true);
	wp_enqueue_script('munich-external', get_template_directory_uri().'/assets/js/external.js', false, '1.0', true);
	wp_enqueue_script('munich-website', get_template_directory_uri().'/assets/js/munich.js', false, '1.0', true);
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    
    /* Munich Google Fonts */
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'munich-googlefonts', "$protocol://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,500,600,700%7CMontserrat:400,700" );
    
    /* Visual Composer Assets 
       (need to manually enqueue them for all pages so the Ajax Navigation can work) */
    wp_enqueue_script( 'jquery_ui_tabs_rotate' );
    wp_enqueue_script( 'wpb_composer_front_js' );
    wp_enqueue_script( 'isotope' );
    wp_enqueue_script( 'prettyphoto' );
	wp_enqueue_script( 'jquery-ui-accordion' );
    wp_enqueue_style( 'prettyphoto' );
    wp_enqueue_style( 'isotope-css' );
    wp_enqueue_style( 'js_composer_front' );
    wp_enqueue_style( 'js_composer_custom_css' );
    
    /* Munich Styles */
	wp_enqueue_style('munich_styles', get_stylesheet_uri(), false, '1.0', 'all');
	wp_enqueue_style('munich_styles_tablet', get_template_directory_uri() . '/assets/css/tablet.css', false, '1.0', 'all');
	wp_enqueue_style('munich_styles_tablet_landscape', get_template_directory_uri() . '/assets/css/tablet_landscape.css', false, '1.0', 'all');
	wp_enqueue_style('munich_styles_desktop', get_template_directory_uri() . '/assets/css/desktop.css', false, '1.0', 'all');
	wp_enqueue_style('munich_m_custom_scrollbar', get_template_directory_uri() . '/assets/css/jquery.mCustomScrollbar.css', false, '1.0', 'all');
	wp_enqueue_style('munic_custom_vc_styles', get_template_directory_uri() . '/assets/css/vc_munich_styles.css', false, '1.0', 'all');
    wp_enqueue_style('munic_custom_woocommerce_styles', get_template_directory_uri() . '/assets/css/woocommerce.css', false, '1.0', 'all');
    
    /* Munich Theme Options Styles */
    add_action('wp_head', 'munich_get_custom_styles');

}
add_action('wp_enqueue_scripts', 'munich_enqueue_assets');


/**
 * Init Visual Composer plugin.
 */
add_action( 'vc_before_init', 'munich_vcSetAsTheme' );
function munich_vcSetAsTheme() {
	if(function_exists("vc_set_as_theme")){
        vc_set_as_theme();
    }
}

/**
 * Hide editor for the gallery template.
 */
function hide_editor() {
    if( isset( $_GET['post'] ) || isset( $_POST['post_ID'] ) ) {
        if( isset( $_GET['post'] ) ) {
            $post_id = $_GET['post'];
        } else {
            $post_id = $_POST['post_ID'];
        }
        
    }else{
        return;
    }
    
    // Remove the editor on the gallery pages
    $template_file = get_post_meta($post_id, '_wp_page_template', true);
    if($template_file == 'gallery.php') remove_post_type_support('page', 'editor');
}
add_action( 'admin_init', 'hide_editor' );


/**
 * HTML tags allowed on the titles and other elements.
 */
$munich_allowed_html_tags = array(
    'a' => array( 'href' => true, 'title' => true ),
    'abbr' => array( 'title' => true ),
    'acronym' => array( 'title' => true ),
    'b' => array(),
    'blockquote' => array( 'cite' => true ),
    'cite' => array(),
    'code' => array(),
    'del' => array( 'datetime' => true ),
    'ins' => array(),
    'em' => array(),
    'i' => array(),
    'q' => array( 'cite' => true ),
    'strike' => array(),
    'strong' => array(),
    'br' => array(),
    'span' => array( 'class' => true ),
    'li' => array( 'class' => true )
);

/**
 * Add Title, Description and Video URL fields to media uploader
 */
function munich_attachment_fields( $form_fields, $post ) {

	$form_fields['munich-gallery-slide-title'] = array(
		'label' => 'Slide Title',
		'input' => 'textarea',
		'value' => get_post_meta( $post->ID, 'munich_gallery_slide_title', true ),
		'helps' => 'Enter the slide\'s title.',
	);
	$form_fields['munich-gallery-slide-description'] = array(
		'label' => 'Slide Description',
		'input' => 'textarea',
		'value' => get_post_meta( $post->ID, 'munich_gallery_slide_description', true ),
		'helps' => 'Enter the slide\'s description.',
	);
	$form_fields['munich-video-url'] = array(
		'label' => 'Video URL',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'munich_video_url', true ),
		'helps' => 'You can enter a Youtube or Vimeo URL.',
	);

	

	return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'munich_attachment_fields', 10, 2 );

/**
 * Store Title, Description and Video URL fields in media uploader
 */
function munich_attachment_fields_save( $post, $attachment ) {
	if( isset( $attachment['munich-gallery-slide-title'] ) )
update_post_meta( $post['ID'], 'munich_gallery_slide_title', $attachment['munich-gallery-slide-title'] );

	if( isset( $attachment['munich-gallery-slide-description'] ) )
update_post_meta( $post['ID'], 'munich_gallery_slide_description', $attachment['munich-gallery-slide-description'] );

	if( isset( $attachment['munich-video-url'] ) )
update_post_meta( $post['ID'], 'munich_video_url', esc_url( $attachment['munich-video-url'] ) );

	return $post;
}
add_filter( 'attachment_fields_to_save', 'munich_attachment_fields_save', 10, 2 );

/**
 * WooCommerce
 */
require get_template_directory() . '/helpers/munich-woocommerce.php';

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 */
function munich_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}
	
	global $page, $paged;

	// Add the blog name
	$title = get_bloginfo( 'name', 'display' ) . $title;

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title .= " $sep " . sprintf( __( 'Page %s', 'munich' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'munich_wp_title', 10, 2 );

/**
 * Handle comments submition.
 */
function munich_ajaxComment($comment_ID, $comment_status) {
	// If it's an AJAX-submitted comment
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		// Get the comment data
		$comment = get_comment($comment_ID);
		// Allow the email to the author to be sent
		wp_notify_postauthor($comment_ID, $comment->comment_type);
		// Get the comment HTML from my custom comment HTML function
		$commentContent = getCommentHTML($comment);
		// Kill the script, returning the comment HTML
		die($commentContent);
	}
}
add_action('comment_post', 'munich_ajaxComment', 20, 2);

/**
 * One-click updates.
 */
require_once(get_template_directory() . '/wp-updates-theme.php');
new WPUpdatesThemeUpdater_1117( 'http://wp-updates.com/api/2/theme', basename( get_template_directory() ) ); 

?>