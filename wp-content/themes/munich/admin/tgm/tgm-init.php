<?php

/**
 * TGM Init Class
 */
include_once ('class-tgm-plugin-activation.php');

function starter_plugin_register_required_plugins() {

	$plugins = array(
		array(
			'name' 		=> 'Redux Framework',
			'slug' 		=> 'redux-framework',
			'required' 	=> true,
		),
        array(
            'name'               => 'Munich Portfolio',
            'slug'               => 'munich-portfolio',
            'source'             => get_template_directory() . '/admin/plugins/munich-portfolio.zip',
            'required'           => true,
            'version'            => '1.1',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => ''
        ),
        array(
            'name'               => 'Munich Blog',
            'slug'               => 'munich-blog',
            'source'             => get_template_directory() . '/admin/plugins/munich-blog.zip',
            'required'           => true,
            'version'            => '1.0',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => ''
        ),
        array(
            'name'               => 'Contact Form 7',
            'slug'               => 'contact-form-7',
            'source'             => get_template_directory() . '/admin/plugins/contact-form-7.zip',
            'required'           => false,
            'version'            => '4.0.1',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => ''
        ),
        array(
            'name'               => 'WPBakery Visual Composer',
            'slug'               => 'js_composer',
            'source'             => get_template_directory() . '/admin/plugins/js_composer.zip',
            'required'           => true,
            'version'            => '4.3.4',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => ''
        ),
        array(
            'name'               => 'Manual Image Crop',
            'slug'               => 'manual-image-crop',
            'source'             => get_template_directory() . '/admin/plugins/manual-image-crop.zip',
            'required'           => false,
            'version'            => '1.08',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => ''
        ),
        array(
            'name'               => 'Post Types Order',
            'slug'               => 'post-types-order',
            'source'             => get_template_directory() . '/admin/plugins/post-types-order.zip',
            'required'           => false,
            'version'            => '1.7.4',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => ''
        )
	);

	$config = array(
		'domain'       		=> 'munich',                  	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_menu_slug' 	=> 'plugins.php', 				// Default parent menu slug
		'parent_url_slug' 	=> 'plugins.php', 				// Default parent URL slug
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
	);

	tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'starter_plugin_register_required_plugins' );