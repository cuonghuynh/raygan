<?php

// Load the global metaboxes helpers
if( file_exists( get_template_directory().'/admin/metaboxes/global.php' ) ) {
    include_once( get_template_directory().'/admin/metaboxes/global.php' );    
}

function get_gallery_metaboxes(){

    $admin_url = get_template_directory_uri();

    $gallery_sections = array();

    if( function_exists( 'add_gallery_metabox_fields' ) ) {
        $gallery_sections = add_gallery_metabox_fields( $gallery_sections, 'gallery_slider' );    
    }

    // Return the meta boxes array
    return array(
        'id'            => 'gallery-options',
        'title'         => __( 'Gallery Options', 'munich' ),
        'post_types'    => array( 'page', 'demo_metaboxes' ),
        'page_template' => array( 'gallery.php' ),
        //'post_format' => array('image'),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $gallery_sections
    );
}