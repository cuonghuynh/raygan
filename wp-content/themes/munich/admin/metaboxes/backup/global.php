<?php

function add_gallery_metabox_fields($slider_sections, $prefix){
	$admin_url = get_template_directory_uri();

	$slider_sections[] = array(
        'title' => __('Slides', 'munich'),
        'desc' => __("In this section you can add and sort the slides of this page's gallery.", 'munich'),
        'icon' => 'el-icon-picture',
        'fields' => array(  
            array(
                'id'          => $prefix.'_slides',
                'type'        => 'gallery',
                'title'       => __('Slides', 'munich'),
                'subtitle'    => __('Add images to the gallery.', 'munich')
            ),
            array(
            	'id'		=> $prefix.'_slides_display_mode',
            	'type'		=> 'radio',
            	'title'		=> __('Display Mode', 'munich'),
            	'subtitle'	=> __('Choose the way you want your slides images to be displayed. <em>Please note that you can force the display method for a particular image. <a href="#">Click here for instructions on how to do that.</a></em>', 'munich'),
            	'options' 	=> array(
            		'cover' 	=> '<img src="'.$admin_url.'/admin/assets/images/icon-cover.png"><strong>Cover:</strong> Your image will be scaled so that the screen is completely covered by the image.',
            		'contain' 	=> '<img src="'.$admin_url.'/admin/assets/images/icon-contain.png"><strong>Contain:</strong> Your image will be scaled to be as large as possible, without cropping any parts of the image.',
            		'auto'		=> '<strong>Auto:</strong> Let the theme decide which method to use: the <em>cover</em> method will be used for landscape images and the <em>contain</em> method will be used for portrait images.'
            	),
            	'default'	=> 'auto'
            )
        )
    );

    $slider_sections[] = array(
        'title' => __('Overlays', 'munich'),
        'desc' => __("The overlays are translucent layers that rendered on top of your slides (a shade) to help you improve the slider navigation controls and title visibility in case you use images with clear tones.", 'munich'),
        'icon' => 'el-icon-photo',
        'fields' => array(  
            array(
                'id'       => $prefix.'_top_overlay',
                'type'     => 'switch',
                'units'    => array('%'),
                'width'    => false,
                'title'    => __('Slider top overlay', 'munich'),
                'subtitle'    => __('This option adds a dark shadow behind the slides titles to improve its visibility (for blog posts galleries for example)', 'munich'),
                'default'  => true,
            ),
            array(
                'id'       => $prefix.'_overlay_height',
                'type'     => 'dimensions',
                'units'    => array('%'),
                'width'    => false,
                'title'    => __('Slider bottom overlay height', 'munich'),
                'subtitle'    => __('This option sets the height of the dark shadow behind the gallery navigation controls.', 'munich'),
                'default'  => array(
                    'height'  => '50'
                ),
            ),
            array(
                'id' => $prefix.'_overlay_opacity',
                'type' => 'slider',
                'title' => __('Slider bottom overlay opacity', 'munich'),
                'subtitle'    => __('This option  sets the opacity of the dark shadow behind the gallery navigation controls.', 'munich'),
                "default" => .4,
                "min" => 0,
                "step" => .1,
                "max" => 1,
                'resolution' => 0.1,
                'display_value' => 'text'
            )
        ),
    );

    $slider_sections[] = array(
        'title' => __('Descriptions', 'munich'),
        'desc' => __("Set the options for the descriptions of your slide's photo.", 'munich'),
        'icon' => 'el-icon-comment',
        'fields' => array(  
            array(
                'id'       => $prefix.'_descriptions_alignment',
                'type'     => 'image_select',
                'title'    => __('Description position', 'munich'),
                'subtitle'    => __('Choose the position where you want your slider\'s photo descriptions to appear.', 'munich'),
                'options'  => array(
                    'bottom-left'      => array(
                        'alt'   => 'Bottom Left', 
                        'img'   => $admin_url.'/admin/assets/images/alignment-icons/align-bottom-left.jpg'
                    ),
                    'bottom-middle'      => array(
                        'alt'   => 'Bottom Middle', 
                        'img'   => $admin_url.'/admin/assets/images/alignment-icons/align-bottom-middle.jpg'
                    ),
                    'bottom-right'      => array(
                        'alt'   => 'Bottom Right', 
                        'img'  => $admin_url.'/admin/assets/images/alignment-icons/align-bottom-right.jpg'
                    )
                ),
                'default'   => 'bottom-middle'
            ),
            array(
                'id'       => $prefix.'_descriptions_background',
                'type'     => 'checkbox',
                'title'    => __('Boxed descriptions?', 'munich'), 
                'subtitle' => __('Enable this option if you want the descriptions to have a dark background.', 'munich'),
                'default'  => '0'// 1 = on | 0 = off
            )
        ),
    );

    $slider_sections[] = array(
        'title' => __('Automatic Slideshow', 'munich'),
        'desc' => __("In this section you can enable or disable the Automatic Slideshow mode for this gallery.", 'munich'),
        'icon' => 'el-icon-youtube',
        'fields' => array(  
            array(
                'id'          => $prefix.'_slideshow_enabled',
                'type'        => 'switch',
                'title'       => __('Automatic Slideshow', 'munich'),
                'subtitle'    => __('When enabled, the slider will animate automatically when the gallery is loaded.', 'munich'),
                'default'     => false
            ),
            array(
                'id'          => $prefix.'_slideshow_speed',
                'type'        => 'spinner',
                'title'       => __('Slideshow speed', 'munich'),
                'subtitle'    => __('The speed of the slideshow cycling, in seconds.', 'munich'),
                'min'         => '0',
                'step'        => '1',
                'max'         => '20',
                'default'     => '5'
            )
        ),
    );

    $slider_sections[] = array(
        'title' => __('Distraction free mode', 'munich'),
        'desc' => __("In this section you can enable or disable the Distraction Free mode for this gallery.", 'munich'),
        'icon' => 'el-icon-eye-open',
        'fields' => array(  
            array(
                'id'          => $prefix.'_distraction_free',
                'type'        => 'switch',
                'title'       => __('Distraction Free mode', 'munich'),
                'subtitle'    => __('Enable or disable the Distraction Free mode for this gallery.', 'munich'),
                'default'     => false
            )
        ),
    );

    return $slider_sections;
}