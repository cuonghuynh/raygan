<?php

function get_custom_page_metaboxes(){

    $admin_url = get_template_directory_uri();

	$custom_page_sections[] = array(
        'icon' => 'el-icon-wrench',
        'fields' => array(  
            array(
                'id'    => 'custom-page-show-title',
                'title' => __( 'Page Title.', 'munich' ),
                'subtitle' => __( 'Show the page title and excerpt on top of the page\'s image.', 'munich' ),
                'type'  => 'switch',
                'default' => true
            ),
            array(
                'id'    => 'custom-page-show-title-shadow',
                'title' => __( 'Shadow behind the title.', 'munich' ),
                'subtitle' => __( 'Show a dark shadow behind the page\'s title to improve legibility.', 'munich' ),
                'type'  => 'switch',
                'default' => true
            )
        )
    );

    // Return the meta boxes array
    return array(
        'id'            => 'custom-page-options',
        'title'         => __( 'Page Options', 'munich' ),
        'post_types'    => array( 'page', 'demo_metaboxes' ),
        'page_template' => array( 'page.php', 'contact.php' ),
        'position'      => 'side', // normal, advanced, side
        'priority'      => 'default', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $custom_page_sections
    );
}
?>