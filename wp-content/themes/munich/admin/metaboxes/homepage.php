<?php

// Load the global metaboxes helpers
if( file_exists( get_template_directory().'/admin/metaboxes/global.php' ) ) {
    include_once( get_template_directory().'/admin/metaboxes/global.php' );    
}

function get_homepage_metaboxes(){

    $admin_url = get_template_directory_uri();

	$homepage_sections[] = array(
        'title' => __('Logo', 'munich'),
        'desc' => __("Set the logo image and its options. This logo will be shown on your homepage only.", 'munich'),
        'icon' => 'el-icon-home',
        'fields' => array(  
            array(
                'id'    => 'homepage_logo',
                'title' => __( 'Image', 'munich' ),
                'subtitle' => __( 'Set an image that will be displayed when you first enter your homepage', 'munich' ),
                'type'  => 'media'
            ),
            array(
                'id'    => 'homepage_logo_retina',
                'title' => __( 'Enable retina logo.', 'munich' ),
                'subtitle' => __( 'Check this option if you have uploaded a double resolution logo for retina displays.', 'munich' ),
                'type'  => 'checkbox',
                'default' => false
            ),
            array(
                'id'       => 'homepage_logo_display',
                'type'     => 'radio',
                'title'    => __('Display options', 'munich'), 
                'subtitle' => __('Choose how you want the logo to be displayed.', 'munich'), 
                'options'  => array (
                    'first-slide-only' => 'On the first slide only',
                    'all-slides' => 'On all the slides'
                ),
                'default'  => 'first-slide-only'
            ),
            array(
                'id' => 'homepage_logo_position_x',
                'type' => 'slider',
                'title' => __('Distance from the left of the page (%)', 'munich'),
                "default" => 50,
                "min" => 0,
                "step" => 1,
                "max" => 100,
                'display_value' => 'text'
            ),
            array(
                'id' => 'homepage_logo_position_y',
                'type' => 'slider',
                'title' => __('Distance from the bottom of the page (%)', 'munich'),
                "default" => 0,
                "min" => 0,
                "step" => 1,
                "max" => 100,
                'display_value' => 'text'
            )
        ),
    );

    if( function_exists( 'add_gallery_metabox_fields' ) ) {
        $homepage_sections = add_gallery_metabox_fields( $homepage_sections, 'homepage_slider' );    
    }

    // Return the meta boxes array
    return array(
        'id'            => 'homepage-options',
        'title'         => __( 'Main Slider Options', 'munich' ),
        'post_types'    => array( 'page', 'demo_metaboxes' ),
        'page_template' => array('homepage.php'),
        //'post_format' => array('image'),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $homepage_sections
    );
}