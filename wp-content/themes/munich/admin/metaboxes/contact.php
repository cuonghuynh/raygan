<?php

function get_contact_metaboxes(){

    $admin_url = get_template_directory_uri();

	$contact_sections[] = array(
        'title'=> "Contact Form",
        'desc' => " ",
        'icon' => 'el-icon-comment-alt',
        'fields' => array(  
            array(
                'id'    => 'contact_page_form_shortcode',
                'title' => __( 'Contact Form 7 Shortcode', 'munich' ),
                'subtitle' => __( 'Enter the sortcode of your contact form.', 'munich' ),
                'type'  => 'text'
            )
        ),
    );
    $contact_sections[] = array(
        'title'=> "Map",
        'desc' => " ",
        'icon' => 'el-icon-map-marker',
        'fields' => array(  
            array(
                'id'    => 'contact_page_map_apy_key',
                'title' => __( 'Google Maps API key', 'munich' ),
                'subtitle' => __( 'Enter your Google Maps API key.', 'munich' ),
                'type'  => 'text',
                'default' => ""
            ),
            array(
                'id'    => 'contact_page_map_lat',
                'title' => __( 'Map Latitude', 'munich' ),
                'subtitle' => __( 'Enter the latitude coordinate of your map', 'munich' ),
                'type'  => 'text',
                'default' => ""
            ),
            array(
                'id'    => 'contact_page_map_lng',
                'title' => __( 'Map Longitude', 'munich' ),
                'subtitle' => __( 'Enter the longitude coordinate of your map.', 'munich' ),
                'type'  => 'text',
                'default' => ""
            ),
            array(
                'id'    => 'contact_page_map_zoom',
                'title' => __( 'Map Zoom', 'munich' ),
                'subtitle' => __( 'Enter the zoom for the map.', 'munich' ),
                'type'  => 'slider',
                'min'   => 0,
                'max'   => 19,
                'default' => 15
            )
        )
    );

    // Return the meta boxes array
    return array(
        'id'            => 'contact-options',
        'title'         => __( 'Contact Page Options', 'munich' ),
        'post_types'    => array( 'page', 'demo_metaboxes' ),
        'page_template' => array('contact.php'),
        //'post_format' => array('image'),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $contact_sections
    );
}