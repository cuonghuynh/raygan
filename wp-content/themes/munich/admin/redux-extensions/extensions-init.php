<?php

$redux_opt_name = "theme_options";

// Place any extra hooks/configs in here for extensions and 
// place the actual extension within the /extensions dir

// Load the Metaboxes extension.
require_once(dirname(__FILE__).'/metaboxes-config.php');

// The loader will load all of the extensions automatically.
// Alternatively you can run the include/init statements below.
require_once(dirname(__FILE__).'/loader.php');