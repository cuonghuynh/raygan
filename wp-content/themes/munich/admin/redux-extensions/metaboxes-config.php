<?php

// INCLUDE THIS BEFORE you load your ReduxFramework object config file.


// You may replace $redux_opt_name with a string if you wish. If you do so, change loader.php
// as well as all the instances below.
$redux_opt_name = "theme_options";


// Example of how metaboxes can work with custom post types
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'acme_product',
        array(
            'labels' => array(
                'name' => __( 'Metaboxes', 'munich' ),
                'singular_name' => __( 'Metabox', 'munich' )
            ),
        'public' => true,
        'has_archive' => true,
        )
    );
}


if ( !function_exists( "redux_add_metaboxes" ) ):
    function redux_add_metaboxes($metaboxes) {
        
        $metaboxes_dir = dirname(dirname(__FILE__)) . '/metaboxes';
        
        // Munich Homepage Metaboxes
        if( file_exists( $metaboxes_dir.'/homepage.php' ) ) {
            require_once( $metaboxes_dir.'/homepage.php' );
            if( function_exists( 'get_homepage_metaboxes' ) ) {
                $metaboxes[] = get_homepage_metaboxes();
            }
        }

        // Munich Gallery Metaboxes
        if( file_exists( $metaboxes_dir.'/gallery.php' ) ) {
            require_once( $metaboxes_dir.'/gallery.php' );
            if( function_exists( 'get_gallery_metaboxes' ) ) {
                $metaboxes[] = get_gallery_metaboxes();
            }
        }
        
        // Munich Contact Page Metaboxes
        if( file_exists( $metaboxes_dir.'/contact.php' ) ) {
            require_once( $metaboxes_dir.'/contact.php' );
            if( function_exists( 'get_contact_metaboxes' ) ) {
                $metaboxes[] = get_contact_metaboxes();
            }
        }
        
        // Munich Custom Page Metaboxes
        if( file_exists( $metaboxes_dir.'/custom-page.php' ) ) {
            require_once( $metaboxes_dir.'/custom-page.php' );
            if( function_exists( 'get_custom_page_metaboxes' ) ) {
                $metaboxes[] = get_custom_page_metaboxes();
            }
        }
    
        /* Munich Portfolio Page and Portfolio Item Metaboxes */
        if( class_exists('MunichPortfolio') ) {
            // Portfolio Page
            $metaboxes[] = MunichPortfolio::get_munich_portfolio_page_metaboxes("munich_portfolio");
            // Portfolio Item
            $metaboxes[] = MunichPortfolio::get_munich_portfolio_item_options_metabox();
            $metaboxes[] = MunichPortfolio::get_munich_portfolio_item_slider_metabox();
        }

        /* Munich Blog Metaboxes */
        if( class_exists('MunichBlog') ) {
            // Portfolio Item
            $metaboxes[] = MunichBlog::get_munich_blog_slider_metabox();
        }

        return $metaboxes;
    }
    add_action('redux/metaboxes/'.$redux_opt_name.'/boxes', 'redux_add_metaboxes');
endif;


// The loader will load all of the extensions automatically based on your $redux_opt_name
require_once(dirname(__FILE__).'/loader.php');