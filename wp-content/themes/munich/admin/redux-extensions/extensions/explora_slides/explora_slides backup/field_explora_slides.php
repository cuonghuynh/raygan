<?php
/**
 * Redux Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Redux Framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Redux Framework. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     ReduxFramework
 * @author      Dovy Paukstys
 * @version     3.1.5
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

// Don't duplicate me!
if( !class_exists( 'ReduxFramework_explora_slides' ) ) {

    /**
     * Main ReduxFramework_explora_slides class
     *
     * @since       1.0.0
     */
    class ReduxFramework_explora_slides extends ReduxFramework {
    
        /**
         * Field Constructor.
         *
         * Required - must call the parent constructor, then assign field and value to vars, and obviously call the render field function
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        function __construct( $field = array(), $value ='', $parent ) {
        
            
            $this->parent = $parent;
            $this->field = $field;
            $this->value = $value;

            if ( empty( $this->extension_dir ) ) {
                $this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
                $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
            }    

            // Set default args for this field to avoid bad indexes. Change this to anything you use.
            $defaults = array(
                'options'           => array(),
                'stylesheet'        => '',
                'output'            => true,
                'enqueue'           => true,
                'enqueue_frontend'  => true,

            );
            $this->field = wp_parse_args( $this->field, $defaults );            
        
        }

        /**
         * Field Render Function.
         *
         * Takes the vars and outputs the HTML for the field in the settings
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function render() {
            echo "welapario";
            // HTML output goes here
            if ( ! empty( $this->field['options'] ) ) {
                echo '<ul>';
                foreach ( $this->field['options'] as $k => $v ) {
                    echo '<li>';
                    // Checkbox label
                    echo '<label for="' . strtr( $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][' . $k . ']', array(
                            '[' => '_',
                            ']' => ''
                        ) ) . '_' . array_search( $k, array_keys( $this->field['options'] ) ) . '">';
                    echo '<input type="hidden" class="checkbox-check" data-val="1" name="' . $this->field['name'] . '[' . $k . ']' . $this->field['name_suffix'] . '" value="' . $this->value[ $k ] . '" ' . '/>';
                    echo '<input type="checkbox" class="checkbox ' . $this->field['class'] . '" id="' . strtr( $this->parent->args['opt_name'] . '[' . $this->field['id'] . '][' . $k . ']', array(
                            '[' => '_',
                            ']' => ''
                        ) ) . '_' . array_search( $k, array_keys( $this->field['options'] ) ) . '" value="1" ' . checked( $this->value[ $k ], '1', false ) . '/>';
                    echo ' ' . $v['label'] . ':</label>';
                    echo '</li>';
                }
                echo '</ul>';
            }

        }
    
        /**
         * Enqueue Function.
         *
         * If this field requires any scripts, or css define this function and register/enqueue the scripts/css
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function enqueue() {

            $extension = ReduxFramework_extension_explora_slides::getInstance();
        
            wp_enqueue_script(
                'redux-field-icon-select-js', 
                $this->extension_url . 'field_explora_slides.js', 
                array( 'jquery' ),
                time(),
                true
            );

            wp_enqueue_style(
                'redux-field-icon-select-css', 
                $this->extension_url . 'field_explora_slides.css',
                time(),
                true
            );
        
        }
        
        /**
         * Output Function.
         *
         * Used to enqueue to the front-end
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */        
        public function output() {

            if ( $this->field['enqueue_frontend'] ) {

            }
            
        }        
        
    }
}
