var MunichAdmin;

(function( $ ) {

	MunichAdmin = {
        options: {
            
        },
		init: function(){
            this.sidebarHeader.init();
		},
        sidebarHeader: {
            options: {
                section: $('fieldset[id$="sidebar_header"]'),
                tabButtons: $('.redux-group-tab-link-li'),
                fields: {
                    image: $('fieldset[id$="sidebar_header_image"]'),
                    text: {
                        icon: $('fieldset[id$="sidebar_header_icon"]'),
                        title: $('fieldset[id$="sidebar_header_title"]'),
                        subtitle: $('fieldset[id$="sidebar_header_subtitle"]')
                    }
                }
            },
            init: function(){
                var section = this.options.section;
                var fields = this.options.fields;
                var tabButtons = this.options.tabButtons;
                
                tabButtons.click( function(){
                    updateHeaderFieldsVisibility();
                    setTimeout(updateHeaderFieldsVisibility, 500);
                });
                section.find('input[type="radio"]').change( updateHeaderFieldsVisibility );

                function updateHeaderFieldsVisibility(){
                    if( section.find('input[value="image"]').is(':checked') ) {
                        showImageField();
                    } else if( section.find('input[value="text"]').is(':checked') ) {
                        showTextFields();
                    }

                    function showImageField(){
                        // Show Image field
                        fields.image.parent().parent().removeClass('portfolio-hidden');
                        // Hide Text fields
                        for (var field in fields.text) {
                            if (fields.text.hasOwnProperty(field)) {
                                fields.text[field].parent().parent().addClass('portfolio-hidden');
                            }
                        }
                    }

                    function showTextFields(){
                        // Show Text fields
                        for (var field in fields.text) {
                            if (fields.text.hasOwnProperty(field)) {
                                fields.text[field].parent().parent().removeClass('portfolio-hidden');
                            }
                        }
                        // Hide Image field
                        fields.image.parent().parent().addClass('portfolio-hidden');
                    }
                }
            }
        }
	}

})(jQuery);

MunichAdmin.init();