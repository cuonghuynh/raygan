<?php

/***************************/
/* Generate Theme Options  */
/***************************/

function munich_add_theme_options($sections) {
    
	$sections[] = munich_theme_options_general();
	$sections[] = munich_theme_options_styling();
    $sections[] = munich_theme_options_featured_works();
    $sections[] = munich_theme_options_blog();
    $sections[] = munich_theme_options_portfolio_page();
    $sections[] = munich_theme_options_shop();
    $sections[] = munich_theme_options_custom_code();
    $sections[] = munich_theme_options_404();
	
	return $sections;
}

/**************************/
/* Theme Options Sections */
/**************************/

/* General */
function munich_theme_options_general(){
    return array(
	    'icon'      => 'el-icon-cogs',
	    'title'     => __('General Settings', 'munich'),
	    'fields'    => array(
	        array(
	            'id'       => 'opt-site-logo',
	            'type'     => 'media',
	            'url'      => true,
	            'title'    => __('Site Logo', 'munich'),
	            'subtitle'     => __('Upload the logo that will be displayed on your main menu.', 'munich'),
	            'default'  => array( 'url'=> get_template_directory_uri() . '/assets/images/default/default-logo.png' )
	        ),
            array(
                'id'    => 'opt-site-logo-retina',
                'title' => __( 'Enable retina logo.', 'munich' ),
                'subtitle' => __( 'Check this option if you have uploaded a double resolution logo for retina displays.', 'munich' ),
                'type'  => 'checkbox',
                'default' => false
            ),
            array(
	            'id'       => 'opt-site-favicon',
	            'type'     => 'media',
	            'url'      => true,
	            'title'    => __('Site Favicon', 'munich'),
	            'desc'     => __('Upload the favicon for your site.', 'munich'),
	            'subtitle' => __('The image should be a .ico image of 32x32 pixels.', 'munich'),
	            'default'  => array( 'url'=> get_template_directory_uri() . '/assets/images/default/default-favicon.ico' )
	        ),
	        array(
	            'id'        => 'opt-nav-text',
	            'type'      => 'editor',
	            'title'     => __('Main Navigation text', 'munich'),
	            'subtitle'  => __('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'munich'),
	            'default'   => 'Insert your main navigation text here.'
	        ),
            array(
	            'id'        => 'munich-analytics-code',
	            'type'      => 'ace_editor',
	            'title'     => __('Analytics tracking code', 'munich'),
	            'subtitle'  => __('Paste your analytis tracking code code here.', 'munich'),
	            'mode'      => 'javascript',
	            'theme'     => 'chrome',
	            'default'   => ""
	        ),
	        array(
	            'id'        => 'opt-social-networks',
	            'type'      => 'sortable',
	            'title'     => __('Social Networks', 'munich'),
	            'subtitle'  => __('Choose the social networks that you want to include on your main menu.<br><br>You need only fill the fields of the social networks you want to show on your main menu.<br><br>You can set the order of the social icons by dragging and dropping the fields.', 'munich'),
	            'mode'      => 'text',
	            'label'     => 'true',
	            'options'   => array(
	                'Facebook' => 'social-facebook',
	                'Twitter' => 'social-twitter',
	                'Behance' => 'social-behance',
	                'Deviantart' => 'social-deviantart',
	                'Instagram' => 'social-instagram',
	                'Picasa' => 'social-picasa',
	                'Vimeo' => 'social-vimeo',
	                'Linkedin' => 'social-linkedin',
	                'Blogger' => 'social-blogger',
	                'Dribble' => 'social-dribble',
	                'Youtube' => 'social-youtube-alt',
	                'Tumblr' => 'social-tumblr',
	                'Path' => 'social-path',
	                '500px' => 'social-500px',
	                'Digg' => 'social-digg',
	                'Dropbox' => 'social-dropbox',
	                'Stumbleupon' => 'social-stumbleupon',
	                'RSS' => 'social-rss',
	                'Yahoo Buzz' => 'social-yahoo-buzz',
	                'Skype' => 'social-skype',
	                'Grooveshark' => 'social-grooveshark',
	                'Google-buzz' => 'social-google-buzz',
	                'Dailybooth' => 'social-dailybooth',
	                'Flickr' => 'social-flickr',
	                'Last' => 'social-last',
	                'Technorati' => 'social-technorati',
	                'Rdio' => 'social-rdio',
	                'Bebo' => 'social-bebo',
	                'Myspace-alt' => 'social-myspace-alt',
	                'Gowalla' => 'social-gowalla',
	                'Github-alt' => 'social-github-alt',
	                'Delicious' => 'social-delicious',
	                'Viddler' => 'social-viddler',
	                'Qik' => 'social-qik',
	                'Zerply' => 'social-zerply',
	                'Yahoo' => 'social-yahoo',
	                'Wordpress' => 'social-wordpress',
	                'Mixx-alt' => 'social-mixx-alt',
	                'Reddit' => 'social-reddit',
	                'Forrst' => 'social-forrst',
	                'Medium' => 'social-medium',
	                'Treehouse' => 'social-treehouse',
	                'Squidoo' => 'social-squidoo',
	                'Spotify' => 'social-spotify',
	                'Hi5' => 'social-hi5',
	                'Bing' => 'social-bing',
	                'Drive' => 'social-drive',
	                'Joomla' => 'social-joomla',
	                'Yelp' => 'social-yelp'
	            ),
	            'default'   => array(
	                'Facebook' => '',
	                'Twitter' => '',
	                'Deviantart' => '',
	                'Instagram' => '',
	                'Picasa' => '',
	                'Vimeo' => '',
	                'Linkedin' => '',
	                'Blogger' => '',
	                'Dribble' => '',
	                'Youtube' => '',
	                'Tumblr' => '',
	                'Path' => '',
	                '500px' => '',
	                'Digg' => '',
	                'Dropbox' => '',
	                'Stumbleupon' => '',
	                'RSS' => '',
	                'Yahoo Buzz' => '',
	                'Skype' => '',
	                'Grooveshark' => '',
	                'Google-buzz' => '',
	                'Dailybooth' => '',
	                'Flickr' => '',
	                'Last' => '',
	                'Technorati' => '',
	                'Rdio' => '',
	                'Bebo' => '',
	                'Myspace-alt' => '',
	                'Gowalla' => '',
	                'Github-alt' => '',
	                'Delicious' => '',
	                'Viddler' => '',
	                'Qik' => '',
	                'Zerply' => '',
	                'Yahoo' => '',
	                'Wordpress' => '',
	                'Mixx-alt' => '',
	                'Reddit' => '',
	                'Forrst' => '',
	                'Medium' => '',
	                'Treehouse' => '',
	                'Squidoo' => '',
	                'Spotify' => '',
	                'Hi5' => '',
	                'Bing' => '',
	                'Drive' => '',
	                'Joomla' => '',
	                'Yelp' => ''
	            )
	        )
	    )
	);
}

/* Styling */
function munich_theme_options_styling(){
       return array(
	    'icon'      => 'el-icon-brush',
	    'title'     => __('Styling Options', 'munich'),
	    'fields'    => array(
	        array(
	            'id'            => 'site-border-color',
	            'type'          => 'color',
	            'title'         => __('Border color', 'munich'),
	            'subtitle'      => __('Select a color for your site\'s border.', 'munich'),
	            'default'       => '#e3e3e3',
	            'transparent'   => false,
	            'validate'      => 'background'
	        ),
            array(
	            'id'            => 'site-primary-color',
	            'type'          => 'color',
	            'title'         => __('Primary color', 'munich'),
	            'subtitle'      => __('Select your site\'s primary color.', 'munich'),
	            'default'       => '#1D212D',
	            'transparent'   => false,
	            'validate'      => 'background'
	        ),
            array(
	            'id'            => 'site-secondary-color',
	            'type'          => 'color',
	            'title'         => __('Secondary color', 'munich'),
	            'subtitle'      => __('Select your site\'s secondary color.', 'munich'),
	            'default'       => '#C93535',
	            'transparent'   => false,
	            'validate'      => 'background'
	        ),
            array(
	            'id'            => 'sidebar-text-color',
	            'type'          => 'color',
	            'title'         => __('Sidebar\'s text color', 'munich'),
	            'subtitle'      => __('Select a color for your sidebar\'s text color.', 'munich'),
	            'default'       => '#858571',
                'transparent'   => false,
	            'validate'      => 'background'
	        ),
            array(
	            'id'            => 'primary-font',
	            'type'          => 'typography',
	            'title'         => __('Primary font', 'munich'),
	            'subtitle'      => __('Select your site\'s posts content font.', 'munich'),
	            'default'       => array(
                    'font-family'   => "Open Sans",
                    'google'        => true
                ),
                'output'        => array('body,input,textarea,select,.posts-gallery .load-more,#cancel-comment-reply-link'),
                'font-style'    => false,
                'font-weight'   => false,
                'font-size'     => false,
                'line-height'   => false,
                'text-align'    => false,
                'color'         => false,
                'subsets'       => false
	        ),
            array(
	            'id'            => 'titles-font',
	            'type'          => 'typography',
	            'title'         => __('Posts Content font', 'munich'),
	            'subtitle'      => __('Select your site\'s titles font.', 'munich'),
	            'default'       => array(
                    'font-family' => "Georgia"
                ),
                'output'        => array('h1, h2, h3, h4, h5, h6, blockquote, #featured-projects .content h6, ul.post-categories li, #main-nav p, #main-nav .nav-text, .munich-gallery .gallery-meta h1, .munich-gallery .gallery-meta h1 *, .posts-gallery li.item .price-inner a em, #work-info .meta li, #related-works nav h1, section.comments .no-comments, .custom-page section.image p, ul#cart-items li span.cart-product-quantity, ul#cart-items li span.cart-product-quantity *, #cart-total .amount, .sidebar .title em'),
                'font-style'    => false,
                'font-weight'   => false,
                'font-size'     => false,
                'line-height'   => false,
                'text-align'    => false,
                'color'         => false,
                'subsets'       => false
	        ),
            array(
	            'id'            => 'secondary-font',
	            'type'          => 'typography',
	            'title'         => __('Secondary font', 'munich'),
	            'subtitle'      => __('Select your site\'s secondary font.', 'munich'),
	            'default'       => array(
                    'font-family' => "Montserrat",
                    'google'      => true
                ),
                'output'        => array('blockquote:before, .wpb_call_to_action .wpb_button, #featured-projects .content a, input[type="submit"], #respond input[type="submit"], #submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit, .woocommerce #content input.button, .woocommerce-page a.button, .woocommerce-page button.button, .woocommerce-page input.button, .woocommerce-page #respond input#submit, .woocommerce-page #content input.button, .woocommerce-page #respond input#submit, .woocommerce-page div.product form.cart .button, .sidebar .title h4, .sidebar .widget h5, .sidebar .pagination span, .munich-gallery .gallery-meta *, ul.post-categories a, .posts-gallery li.item span.price span, #work-info .meta li strong, #related-works nav h1 strong, .post .date, section.comments h6, #cart-toggle-wrapper span, #cart-qty .amount, #cart-links a, #cart-total, .related.products h2, .munich-gallery .posts-nav a, #featured-works-label, .blog-posts .post a.read-more'),
                'font-style'    => false,
                'font-weight'   => false,
                'font-size'     => false,
                'line-height'   => false,
                'text-align'    => false,
                'color'         => false,
                'subsets'       => false
	        )
	    )
	);
}

/* Featured Works */
function munich_theme_options_featured_works(){
    if( class_exists('MunichPortfolio') ) {
        return array(
            'icon'      => 'el-icon-star',
            'title'     => __('Featured Projects', 'munich'),
            'fields'	=> array(
                array(
                    'id'        => 'munich_featured_works',
                    'type'      => 'checkbox',
                    'title'     => __('Featured Projects', 'munich'),
                    'subtitle' => __("Choose which projects to show on the featured projects area.", 'munich'), 
                    'data'     => 'posts',
                    'args'     => array('post_type'=>'portfolio', 'posts_per_page' => -1)
                )
            )
        );
    } else {
        return array();   
    }
    
}

/* Blog */
function munich_theme_options_blog(){
    return array(
        'icon'      => 'el-icon-rss',
        'title'     => __('News Announcement', 'munich'),
        'fields'	=> munich_theme_options_sidebar_header( "munich_blog" )
    );
}

/* Portfolio */
function munich_theme_options_portfolio_page(){
    if( class_exists('MunichPortfolio') ) {
        return MunichPortfolio::munich_portfolio_get_theme_options("munich_portfolio_archive");
    } else {
        return array();   
    }
}

/* Shop */
function munich_theme_options_shop(){
    return array(
        'icon'      => 'el-icon-folder-open',
        'title'     => __('Press Release', 'munich'),
        'fields'	=> array_merge( array(
                array(
                    'id'        => 'munich_shop_image',
                    'type'      => 'media',
                    'title'     => __('Press Release default image', 'munich'),
                    'subtitle' => __("Choose an image to display on the left side of all Press Release pages.", 'munich')
                ),
                array(
                    'id'       => 'munich_shop_show_overlays',
                    'type'     => 'radio',
                    'title'    => __('Press Release Archive items overlay', 'munich'), 
                    'subtitle' => __('Do you want to display the dark overlay on top of the Press Release?', 'munich'), 
                    'options'  => array (
                        'visible' => 'Visible',
                        'hidden' => 'Hidden'
                    ),
                    'default'  => 'visible'
                )
            ),                  
            munich_theme_options_sidebar_header( "munich_shop" )
        )
    );
}

/* Custom Code */
function munich_theme_options_custom_code(){
    return array(
	    'title'     => __('Custom Code', 'munich'),
	    'desc'      => __('Customize your theme with custom CSS and JavaScript', 'munich'),
	    'icon'      => 'el-icon-file-edit ',
	    'fields'    => array(
	        array(
	            'id'        => 'munich-custom-css',
	            'type'      => 'ace_editor',
	            'title'     => __('CSS Code', 'munich'),
	            'subtitle'  => __('Paste your CSS code here.', 'munich'),
	            'mode'      => 'css',
	            'theme'     => 'monokai',
	            'desc'      => 'The &lt;style&gt; tag will be added automatically for you.',
	            'default'   => ""
	        ),
	        array(
	            'id'        => 'munich-custom-javascript',
	            'type'      => 'ace_editor',
	            'title'     => __('Custom Javascript', 'munich'),
	            'subtitle'  => __('Paste your custom JavaScript code here.', 'munich'),
	            'mode'      => 'javascript',
	            'theme'     => 'chrome',
	            'desc'      => "Please don't forget to include your &lt;script&gt; tags.",
	            'default'   => ""
	        )
	    ),
	);
}

/* Custom Code */
function munich_theme_options_404(){
    return array(
	    'title'     => __('404 Page Options', 'munich'),
	    'desc'      => __('Customize your 404 (Not Found) page', 'munich'),
	    'icon'      => 'el-icon-warning-sign',
	    'fields'    => array(
	        array(         
                'id'       => 'munich-404-background',
                'type'     => 'background',
                'title'    => __('Page Background', 'redux-framework-demo'),
                'default'  => array(
                    'background-color' => '#282d3d'
                )
            ),
            array(
                'id' => 'munich-404-opacity',
                'type' => 'slider',
                'title' => __('404 message opacity', 'redux-framework-demo'),
                "default" => 0.5,
                "min" => 0,
                "step" => .1,
                "max" => 1,
                'resolution' => 0.1,
                'display_value' => 'text'
            )
	    ),
	);
}

/***************************/
/* Shared Elements Options */
/***************************/

function munich_theme_options_sidebar_header( $prefix ) {
    return array(  
        array(
            'id'       => $prefix . '_sidebar_visibility',
            'type'     => 'radio',
            'title'    => __('Sidebar Visibility', 'munich'), 
            'subtitle' => __('Do you want the filtering sidebar to be visible on this  page?', 'munich'), 
            'options'  => array (
                'visible' => 'Visible',
                'hidden' => 'Hidden'
            ),
            'default'  => 'visible'
        ),
        array(
           'id' => 'section-start',
           'type' => 'section',
           'title' => __('Sidebar Header', 'munich'),
           'indent' => true 
        ),
        array(
            'id'       => $prefix . '_sidebar_header',
            'type'     => 'radio',
            'title'    => __('Sidebar Header Type', 'munich'), 
            'subtitle' => __('You can choose to show text on your sidebar\'s header, or an image.', 'munich'), 
            'options'  => array (
                'text' => 'Text (with an optional icon)',
                'image' => 'Image'
            ),
            'default'  => 'text'
        ),
        array(
            'id'       => $prefix . '_sidebar_header_image',
            'type'     => 'media',
            'title'    => __('Sidebar Header Image', 'munich'), 
            'subtitle' => __('Choose an image/logo to show on the right sidebar of your portfolio page.', 'munich')
        ),
        array(
            'id'       => $prefix . '_sidebar_header_icon',
            'type'     => 'image_select',
            'title'    => __('Sidebar Header Icon', 'munich'), 
            'subtitle' => __('Icon to show on top of your sidebar text.', 'munich'),
            'options'  => get_icons_fields('sidebar_icons'),
            'default'  => 'icon-star'
        ),
        array(
            'id'       => $prefix . '_sidebar_header_title',
            'type'     => 'text',
            'title'    => __('Sidebar Header Title', 'munich'), 
            'default'  => 'LIST OF PROJECTS'
        ),
        array(
            'id'       => $prefix . '_sidebar_header_subtitle',
            'type'     => 'text',
            'title'    => __('Sidebar Header Subtitle', 'munich'), 
            'default'  => 'Hope you enjoy the view!'
        ),
        array(
            'id'     => 'section-end',
            'type'   => 'section',
            'indent' => false,
        )
    );
}

/***************************/
/*      Helper Methods     */
/***************************/

function get_icons_fields($folder_name = "", $path = "", $path_url = "") {
    if( strlen( $path ) == 0 ) $path = get_template_directory() . '/assets/images/';
    if( strlen( $path_url ) == 0 ) $path_url = get_template_directory_uri() . '/assets/images/';
    if( strlen( $folder_name ) == 0 ) $folder_name = 'icons';

    $icons = scandir( $path . $folder_name );
    $icons_fields = array();
    if( is_array( $icons ) ) {
        foreach ($icons as $icon) {
            if( strpos($icon, ".png") ) {
                $icon_name = str_replace(".png", "", $icon);
                $icons_fields[$icon_name] = array(
                    'alt'   => $icon_name,
                    'img'   => $path_url . $folder_name . '/' . $icon
                );
            }
        }
    }
    return $icons_fields;
}