<?php

// Remove editor support for the homepage template
add_action('init', 'remove_editor_init');
function remove_editor_init() {
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } else if (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    } else {
        return;
    }
    $template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
    if ($template_file == 'homepage.php') {
        remove_post_type_support('page', 'editor');
        remove_post_type_support('page', 'thumbnail');
    }
}