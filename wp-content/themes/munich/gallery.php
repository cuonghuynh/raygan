<?php
/*
Template Name: Gallery Slider
*/

get_header();

if ( !post_password_required( $post ) ) { ?>
<div id="page-body" class="gallery gallery-page">
    <?php 
    // Gallery options
    $prefix = "gallery";
    $has_logo = false; 
    $keep_aspect_ratio = false;
    $has_thumbs = true;
    // Call gallery
    include(locate_template( 'partials/modules/gallery.php' )); 
    ?>
</div>
<?php 
} else {
    echo get_the_password_form();
}

get_footer(); ?>
