
<?php
global $theme_options;
global $munich_allowed_html_tags;
$list = '';
$networks = array(
    'facebook' => 'social-facebook',
    'twitter' => 'social-twitter',
    'behance' => 'social-behance',
    'deviantart' => 'social-deviantart',
    'instagram' => 'social-instagram',
    'picasa' => 'social-picasa',
    'vimeo' => 'social-vimeo',
    'linkedin' => 'social-linkedin',
    'blogger' => 'social-blogger',
    'dribble' => 'social-dribble',
    'youtube' => 'social-youtube-alt',
    'tumblr' => 'social-tumblr',
    'path' => 'social-path',
    '500px' => 'social-500px',
    'digg' => 'social-digg',
    'dropbox' => 'social-dropbox',
    'stumbleupon' => 'social-stumbleupon',
    'RSS' => 'social-rss',
    'skype' => 'social-skype',
    'grooveshark' => 'social-grooveshark',
    'dailybooth' => 'social-dailybooth',
    'flickr' => 'social-flickr',
    'last' => 'social-last',
    'technorati' => 'social-technorati',
    'rdio' => 'social-rdio',
    'bebo' => 'social-bebo',
    'myspace' => 'social-myspace-alt',
    'gowalla' => 'social-gowalla',
    'github' => 'social-github-alt',
    'delicious' => 'social-delicious',
    'viddler' => 'social-viddler',
    'qik' => 'social-qik',
    'zerply' => 'social-zerply',
    'yahoo' => 'social-yahoo',
    'wordpress' => 'social-wordpress',
    'mixx' => 'social-mixx-alt',
    'reddit' => 'social-reddit',
    'forrst' => 'social-forrst',
    'medium' => 'social-medium',
    'treehouse' => 'social-treehouse',
    'squidoo' => 'social-squidoo',
    'spotify' => 'social-spotify',
    'hi5' => 'social-hi5',
    'bing' => 'social-bing',
    'drive' => 'social-drive',
    'joomla' => 'social-joomla',
    'yelp' => 'social-yelp'
);
if(isset($theme_options['opt-social-networks'])) {
    foreach ($theme_options['opt-social-networks'] as $key => $value){
        if ( $value ) {
            foreach($networks as $url => $class) {
                if( strpos($value, $url) !== false  ) {
                    $list .= '<li class="icon-' . strtolower( esc_attr( $class ) ) . '"><a target="_blank" href="' . esc_attr( $value ) . '"></a></li>';         
                }
            }
        } 
    } 
    if ( $list ) { ?>
<div class="social-networks-wrapper">
	<ul class="social-networks">
	<?php echo wp_kses( $list, $munich_allowed_html_tags ); ?>
	</ul>
</div>
<?php
    }
}
?>
