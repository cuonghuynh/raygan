<?php 
    if( function_exists( 'redux_post_meta') ) {
        $mainSlider = redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_slides" );
        
        $mobileSlider = redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_slides_mobile" );

        $overlay_height = redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_overlay_height" );
        $slider_options = (object) array (
            'descriptions_alignment'	=> 	redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_descriptions_alignment" ),
            'descriptions_boxed'		=> 	redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_descriptions_background" ),
            'top_overlay'			    =>	redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_top_overlay" ),
            'overlay_height'			=>	$overlay_height[ 'height' ],
            'overlay_opacity'			=>	redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_overlay_opacity" ),
            'distraction_free'			=>	redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_distraction_free" ),
            'display_mode'				=>	redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_slides_display_mode" ),
            'slideshow'					=>	redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_slideshow_enabled" ),
            'slideshow_speed'			=>	redux_post_meta( "theme_options", $post->ID, $prefix . "_slider_slideshow_speed" )
        );
    } else {
        $mainSlider = "";
        $slider_options = (object) array(
            'descriptions_alignment'	=> 	"bottom-middle",
            'descriptions_boxed'		=> 	"0",
            'top_overlay'			    =>	true,
            'overlay_height'			=>	"50%",
            'overlay_opacity'			=>	"0.4",
            'distraction_free'			=>	false,
            'display_mode'				=>	"cover",
            'slideshow'					=>	false,
            'slideshow_speed'			=>	5
        );
    }
    
	if( !isset( $has_thumbs ) ) { $has_thumbs = false; }
    global $munich_allowed_html_tags;
?>
<!-- for desktop mode	 -->
<div id="main-slider-desktop" data-auto-slideshow="<?php echo esc_attr( $slider_options->slideshow ); ?>" data-slideshow-speed="<?php echo esc_attr( $slider_options->slideshow_speed ); ?>" class="munich-gallery desktop backgrounds-<?php echo esc_attr( $slider_options->display_mode ); ?> on-first-slide <?php if( $has_thumbs ){ echo "thumb-nav"; } ?> <?php echo ($keep_aspect_ratio) ? "keep-aspect-ratio" : "fixed-aspect-ratio"; ?> desc-align-<?php echo esc_attr( $slider_options->descriptions_alignment ); ?> desc-box-<?php echo esc_attr( $slider_options->descriptions_boxed ); ?> <?php if($slider_options->distraction_free){ echo "distraction-free"; } ?> <?php if($slider_options->top_overlay){ echo "top-overlay"; } ?>">
	<span class="scroll-animation visible"></span>
	<?php if( isset( $meta_info ) ) { ?>
	<div class="gallery-meta">
		<h1><?php echo wp_kses( $meta_info->title, $munich_allowed_html_tags );  ?></h1>
		<span class="date"><?php echo esc_html( $meta_info->date ); ?></span>
	</div>
	<?php } ?>
	<ul class="slides">
	<?php
	if( strlen($mainSlider) == 0 ) {
		// If the gallery is empty, use the featured image instead.
		$mainSlider = strval( get_post_thumbnail_id( get_the_ID() ) );
	};

	$backgroundSize = $slider_options->display_mode;
	$mainSlider = explode( ",", $mainSlider );
	
	// loop for slider desktop
	foreach ($mainSlider as $key => $value) {
		$attachment = get_post( $value );
	

		$slide = wp_get_attachment_image_src( $value, "full" );
		$slide = array(
			'image' => $slide[0],
			'height' => $slide[2],
			'width' => $slide[1],
			'title' => get_post_meta($value, 'munich_gallery_slide_title', true),
			'description' => get_post_meta($value, 'munich_gallery_slide_description', true),
			'video' => get_post_meta($value, 'munich_video_url', true)
		);

		if($backgroundSize == "auto") {
			$slideBackgroundSize = "cover";
			if( intval($slide['height']) > intval($slide['width']) ) $slideBackgroundSize = "contain";
		} else {
			$slideBackgroundSize = $backgroundSize;
		}
	?>
		<li class="slide desktop preload not-done" data-image="<?php echo esc_attr( $slide['image'] ); ?>" data-video="<?php echo esc_attr( $slide['video'] ); ?>" style="background-size: <?php echo esc_attr( $slideBackgroundSize ); ?>">
			<div class="aspect-ratio" style="padding-top: <?php echo (intval($slide['height']) / intval($slide['width'])) * 100; ?>%"></div>
			<?php if( strlen($slide['title']) > 0 || strlen($slide['description']) > 0 ) { ?>
			<div class="description">
				<h6><?php echo wp_kses( $slide['title'], $munich_allowed_html_tags );  ?></h6>
				<p><?php echo wp_kses( $slide['description'], $munich_allowed_html_tags ); ?></p>
			</div>
			<?php } ?>
			<div class="overlay" style="height: <?php echo esc_attr( $slider_options->overlay_height ); ?>; opacity: <?php echo esc_attr( $slider_options->overlay_opacity ); ?>;"></div>
			<?php if( strlen($slide['video']) > 0 ) { ?>
			<div class="video-overlay"></div>
			<button class="video-play"></button>
			<?php } ?>
		</li>
	<?php } ?>
	
	</ul>
	<div class="slides-nav desktop">
		<span class="prev"><</span>
		<span class="next">></span>
	</div>
	<?php 
	if( $has_logo ) {
        $logo_url = "";
        $logo_width = 0;
        if(is_array(redux_post_meta( "theme_options", $post->ID, $prefix . "_logo" ))){
            $logo_array = redux_post_meta( "theme_options", $post->ID, $prefix . "_logo" );
            $logo_url = $logo_array[ 'url' ];
            $logo_width = intval( $logo_array[ 'width' ] );
        }
		$logo = (object) array(
			'url'			=> $logo_url,
			'display'		=> redux_post_meta( "theme_options", $post->ID, $prefix . "_logo_display" ),
			'width'			=> $logo_width,
			'position_x'	=> redux_post_meta( "theme_options", $post->ID, $prefix . "_logo_position_x" ),
			'position_y'	=> redux_post_meta( "theme_options", $post->ID, $prefix . "_logo_position_y" ),
            'retina'	    => redux_post_meta( "theme_options", $post->ID, $prefix . "_logo_retina" )
        );
        $logo_width = $logo->width;
        if($logo->retina == 1) { $logo_width = $logo_width / 2; }
    ?>
	<div id="<?php echo esc_attr( $prefix ); ?>-logo-wrapper">
		<img id="<?php echo esc_attr( $prefix ); ?>-logo" 
			class="<?php echo esc_attr( $logo->display ); ?> visible" 
			src="<?php echo esc_attr( $logo->url ); ?>" 
			alt="<?php _e('Site Logo', 'munich'); ?>" 
			style="width: <?php echo esc_attr( $logo_width ); ?>px; height: auto; bottom: <?php echo esc_attr( $logo->position_y ); ?>%; left: <?php echo esc_attr( $logo->position_x ); ?>%;" />
	</div>
	<?php } ?>	
	<?php if( isset( $posts_navigation ) && $posts_navigation ){ ?>
	<div class="posts-nav">
		<?php 
		next_post_link('<div class="newer icon icon-arrow-1-left">%link</div>', __('NEWER POST', 'munich')); 
		previous_post_link('<div class="older icon icon-arrow-1-right">%link</div>', __('OLDER POST', 'munich')); 
		?>
	</div>
	<?php } ?>
</div> <!-- /for desktop mode -->
<!-- for mobile mode -->
<div id="main-slider-mobile" data-auto-slideshow="<?php echo esc_attr( $slider_options->slideshow ); ?>" data-slideshow-speed="<?php echo esc_attr( $slider_options->slideshow_speed ); ?>" class="munich-gallery mobile backgrounds-<?php echo esc_attr( $slider_options->display_mode ); ?> on-first-slide <?php if( $has_thumbs ){ echo "thumb-nav"; } ?> <?php echo ($keep_aspect_ratio) ? "keep-aspect-ratio" : "fixed-aspect-ratio"; ?> desc-align-<?php echo esc_attr( $slider_options->descriptions_alignment ); ?> desc-box-<?php echo esc_attr( $slider_options->descriptions_boxed ); ?> <?php if($slider_options->distraction_free){ echo "distraction-free"; } ?> <?php if($slider_options->top_overlay){ echo "top-overlay"; } ?>">
	<span class="scroll-animation visible"></span>
	<?php if( isset( $meta_info ) ) { ?>
	<div class="gallery-meta">
		<h1><?php echo wp_kses( $meta_info->title, $munich_allowed_html_tags );  ?></h1>
		<span class="date"><?php echo esc_html( $meta_info->date ); ?></span>
		<?php if( strlen( $meta_info->author ) ) { ?>
		<span class="author"><?php echo esc_html( $meta_info->author ); ?></span>
		<?php } ?>
	</div>
	<?php } ?>
	<ul class="slides">

	<?php
	if( strlen($mobileSlider) == 0 ) {
		// If the gallery is empty, use the featured image instead.
		$mobileSlider = strval( get_post_thumbnail_id( get_the_ID() ) );
	};

	$backgroundSize = $slider_options->display_mode;
	$mobileSlider = explode( ",", $mobileSlider );
	
	// loop for slider desktop
	foreach ($mobileSlider as $key => $value) {
		$attachment = get_post( $value );
	

		$slide = wp_get_attachment_image_src( $value, "full" );
		$slide = array(
			'image' => $slide[0],
			'height' => $slide[2],
			'width' => $slide[1],
			'title' => get_post_meta($value, 'munich_gallery_slide_title', true),
			'description' => get_post_meta($value, 'munich_gallery_slide_description', true),
			'video' => get_post_meta($value, 'munich_video_url', true)
		);

		if($backgroundSize == "auto") {
			$slideBackgroundSize = "cover";
			if( intval($slide['height']) > intval($slide['width']) ) $slideBackgroundSize = "contain";
		} else {
			$slideBackgroundSize = $backgroundSize;
		}
	?>
		<li class="slide desktop preload not-done" data-image="<?php echo esc_attr( $slide['image'] ); ?>" data-video="<?php echo esc_attr( $slide['video'] ); ?>" style="background-size: <?php echo esc_attr( $slideBackgroundSize ); ?>">
			<div class="aspect-ratio" style="padding-top: <?php echo (intval($slide['height']) / intval($slide['width'])) * 100; ?>%"></div>
			<?php if( strlen($slide['title']) > 0 || strlen($slide['description']) > 0 ) { ?>
			<div class="description">
				<h6><?php echo wp_kses( $slide['title'], $munich_allowed_html_tags );  ?></h6>
				<p><?php echo wp_kses( $slide['description'], $munich_allowed_html_tags ); ?></p>
			</div>
			<?php } ?>
			<div class="overlay" style="height: <?php echo esc_attr( $slider_options->overlay_height ); ?>; opacity: <?php echo esc_attr( $slider_options->overlay_opacity ); ?>;"></div>
			<?php if( strlen($slide['video']) > 0 ) { ?>
			<div class="video-overlay"></div>
			<button class="video-play"></button>
			<?php } ?>
		</li>
	<?php } ?>
	
	</ul>
	<div class="slides-nav mobile">
		<span class="prev"><</span>
		<span class="next">></span>
	</div>
	<?php 
	if( $has_logo ) {
        $logo_url = "";
        $logo_width = 0;
        if(is_array(redux_post_meta( "theme_options", $post->ID, $prefix . "_logo" ))){
            $logo_array = redux_post_meta( "theme_options", $post->ID, $prefix . "_logo" );
            $logo_url = $logo_array[ 'url' ];
            $logo_width = intval( $logo_array[ 'width' ] );
        }
		$logo = (object) array(
			'url'			=> $logo_url,
			'display'		=> redux_post_meta( "theme_options", $post->ID, $prefix . "_logo_display" ),
			'width'			=> $logo_width,
			'position_x'	=> redux_post_meta( "theme_options", $post->ID, $prefix . "_logo_position_x" ),
			'position_y'	=> redux_post_meta( "theme_options", $post->ID, $prefix . "_logo_position_y" ),
            'retina'	    => redux_post_meta( "theme_options", $post->ID, $prefix . "_logo_retina" )
        );
        $logo_width = $logo->width;
        if($logo->retina == 1) { $logo_width = $logo_width / 2; }
    ?>
	<div id="<?php echo esc_attr( $prefix ); ?>-logo-wrapper">
		<img id="<?php echo esc_attr( $prefix ); ?>-logo" 
			class="<?php echo esc_attr( $logo->display ); ?> visible" 
			src="<?php echo esc_attr( $logo->url ); ?>" 
			alt="<?php _e('Site Logo', 'munich'); ?>" 
			style="width: <?php echo esc_attr( $logo_width ); ?>px; height: auto; bottom: <?php echo esc_attr( $logo->position_y ); ?>%; left: <?php echo esc_attr( $logo->position_x ); ?>%;" />
	</div>
	<?php } ?>	
	<?php if( isset( $posts_navigation ) && $posts_navigation ){ ?>
	<div class="posts-nav">
		<?php 
		next_post_link('<div class="newer icon icon-arrow-1-left">%link</div>', __('NEWER POST', 'munich')); 
		previous_post_link('<div class="older icon icon-arrow-1-right">%link</div>', __('OLDER POST', 'munich')); 
		?>
	</div>
	<?php } ?>
</div>
<!-- /for mobile mode -->
<?php if( $has_thumbs ) { ?>
<nav class="munich-gallery-thumbs"><ul class="inner"><!--
--><?php foreach ($mainSlider as $key => $value) { 
			$slide = wp_get_attachment_image_src( $value, "medium" ); ?><!--
	--><li class="item preload" data-image="<?php echo esc_attr( $slide[0] ); ?>"><div class="progress"></div></li><!--
 --><?php } ?><!--
--></ul><div class="active-thumb"></div></nav>
<?php } ?>