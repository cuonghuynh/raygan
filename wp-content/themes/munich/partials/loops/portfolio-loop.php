<?php

include( get_template_directory() . "/helpers/portfolio-page-variables.php" );
    
// The QUERY

$args = array (
	'post_type' 		=> 'portfolio',
	'posts_per_page'	=> -1,
	'orderby' 			=> 'date',
	'order' 			=> 'DESC'
);

if ( sizeof( $query_terms ) > 0 ) {
	$terms_args = array (
		'tax_query' => array(
			array(
				'taxonomy' 	=> 'work_type',
				'field'		=> 'slug',
				'terms'		=> $query_terms
			)
		)
	);

	$args = array_merge($args, $terms_args);
}

$works = new WP_Query($args);

?>

<div id="page-body" class="portfolio posts-gallery theme-<?php echo esc_attr( $portfolio_options->theme ); ?> sidebar-<?php echo esc_attr( $portfolio_options->sidebar_visibility ); ?> overlays-<?php echo esc_attr( $portfolio_options->show_overlays ); ?>">
	<div class="flexslider">
		<section id="portfolio-list" class="slides">
			<div class="image-spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
			<ul class="portfolio-page posts-gallery-page" id="page-tmp">
	<?php if ( $works->have_posts() ) :
			while ( $works->have_posts() ) : $works->the_post();
				$displayOptions = (object) array(
					'block_type'	=> 	redux_post_meta( "theme_options", $post->ID, "munich_portfolio_block_type" )
                );
				$thumb_src = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $displayOptions->block_type . '_thumb' );
				$terms = get_the_terms( get_the_ID(), 'work_type' );
				if( gettype($displayOptions->block_type) != 'string' ) { $displayOptions->block_type = 'portrait'; }
			?>
				<li class="portfolio-item item <?php echo esc_attr( $displayOptions->block_type ); ?>">
                    <div class="overlay"></div>
					<div class="image preload" data-image="<?php echo esc_attr( $thumb_src[0] ); ?>"></div>
					<div class="description">
                        <form action="<?php the_permalink(); ?>" method="post">
                            <input type="hidden" name="portfolio-page-url" value="#" />
                            <a href="<?php the_permalink(); ?>">
                                <span class="icon icon-arrow-right"></span>
                                <h5><?php echo the_title(); ?></h5>
                            </a>
                        </form>
						
						<?php
						if($terms && sizeof($terms) > 0) { ?>
						<ul class="post-categories">
						<?php
							foreach ( $terms as $term ) { 
								$term_url = $work_type_url . $term->slug;
								if ( $is_archive ) $term_url = get_term_link( $term, 'work_type' ); ?>
								<li><a href="<?php echo esc_attr( $term_url ); ?>"><?php echo esc_html( $term->slug ); ?></a></li>
	<?php					} ?>
						</ul>
	<?php				} ?>
					</div>
				</li>
	<?php	endwhile;		
		endif; ?>
			</ul>
		</section>
	</div>
    <?php wp_reset_query();  ?>
	<nav class="sidebar">
		<div class="wrapper">
			<?php get_sidebar('portfolio') ?>
		</div>
		<div class="pagination">
			<span id="current-page" data-current-page="1">
				<span>1</span><span>/</span><span>1</span>
			</span>
			<button id="prev-page" class="icon icon-arrow-65"></button>
			<button id="next-page" class="icon icon-arrow-66"></button>
		</div>
	</nav>
</div>