<?php

	$per_page = 4;
	$current_page = 1;
	$current_index = 1;
	$new_page = false;

	$supported_terms = get_terms( 'category' );

	//create post data form query function:
	query_posts('cat=12&orderby=desc');

?>

<div id="page-body" class="blog posts-gallery loading">
    <div class="image-spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
	<div class="flexslider">
		<section class="blog-posts slides">
		<?php if ( have_posts() ) : ?>
			<ul class="blog-page page visible" id="page-1">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php  
			
			if( $current_index > $per_page ) {
				$current_page++;
				$current_index = 1;
				$new_page = true;
			}

			?>

			<?php if( $new_page ) { ?>
			</ul>
			<ul class="blog-page page" id="page-<?php echo esc_attr( $current_page ); ?>">
			<?php 
					$new_page = false;
			} ?>
				<li <?php post_class("post top"); ?>>
					<?php
                    $featured_image = wp_get_attachment_url( get_post_thumbnail_id() );
                    $featured_image_class = "image";
                    if( strlen( $featured_image ) == 0 ) $featured_image_class = "image no-image";
                    ?>
					<div class="<?php echo esc_attr( $featured_image_class ); ?> post-header" data-image="<?php echo esc_attr( $featured_image ); ?>">
						<p><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></p>
					</div>
					<div class="info">
						<div class="date">
							<div class="inner">
								<span class="month"><?php echo get_the_date('M'); ?></span>
								<span class="day"><?php echo get_the_date('d'); ?></span>
								<span class="year"><?php echo get_the_date('Y'); ?></span>
							</div>
						</div>
						<div class="content">
							<h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                           <!--  <?php the_category(); ?> -->
							<div class="excerpt">
								<?php the_excerpt(); ?>
							</div>
						</div>
					</div>
					<a href="<?php echo get_permalink(); ?>" class="read-more"><?php _e('READ MORE', 'munich'); ?></a>
				</li>
				<?php $current_index++; ?>
		<?php endwhile; ?>
			</ul>

		<?php else: ?>	

			<article class="no-results">
				<h1><strong><?php _e('Sorry!', 'munich'); ?></strong></h1>
                <h4><?php _e('No posts matched your criteria.', 'munich'); ?></h4>
				<?php the_widget('WP_Widget_Search'); ?>
			</article>

		<?php endif; ?>
            <div id="paginate-links"><?php paginate_links(); ?></div>
		</section>
	</div>
</div>
<button id="toggle-sidebar" class="toggle-button">
	<span class="bar first"></span>
	<span class="bar middle"></span>
	<span class="bar last"></span>
</button>
<section class="sidebar blog-sidebar">
	<div class="wrapper">
		<?php get_sidebar(); ?>
	</div>
	<div class="pagination">
		<span id="current-page" data-current-page="1" data-pages="<?php echo esc_attr( $current_page ); ?>">
			<span>1</span><span>/</span><span><?php echo esc_html( $current_page ); ?></span>
		</span>
		<button id="prev-page" class="icon icon-arrow-65"></button>
		<button id="next-page" class="icon icon-arrow-66"></button>
	</div>
</section>