<?php get_header(); ?>
<div id="page-body" class="error404">
    <div class="error-404">
        <h1>4<span class="error-0"></span>4</h1>
        <div class="inner">
            <span class="oops"><?php _e("Oops!", 'munich'); ?></span>
            <p><?php _e("The page you are looking for doesn't exist.", 'munich'); ?></p>
        </div>
    </div>
</div>
<?php get_footer(); ?>