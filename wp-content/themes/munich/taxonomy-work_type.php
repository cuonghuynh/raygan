<?php

get_header();

if ( !post_password_required( $post ) ) {
	
	$is_archive = true;

	$query_terms = array( get_query_var( 'term' ) ); // An empty array means all terms are queried.
	$supported_terms = get_terms( 'work_type' ); // All terms are supported on the archive page.

	$work_type_url = '';

	$page_ID = 0;

	include(locate_template('partials/loops/portfolio-loop.php'));

} else {
	echo get_the_password_form();
}

get_footer();

?>