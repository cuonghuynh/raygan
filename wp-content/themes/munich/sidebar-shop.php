<?php 
// global $woocommerce; global $theme_options;
// $items = $woocommerce->cart->get_cart();
// $cart_empty = ( sizeof( $items ) == 0 );
global $theme_options;
$shop_sidebar_options = (object) array(
    'sidebar_visibility'	=> $theme_options['munich_shop_sidebar_visibility'],
    'header_type'			=> $theme_options['munich_shop_sidebar_header'],
    'header_icon'			=> $theme_options['munich_shop_sidebar_header_icon'],
    'header_title'			=> $theme_options['munich_shop_sidebar_header_title'],
    'header_subtitle'		=> $theme_options['munich_shop_sidebar_header_subtitle'],
    'header_image'			=> $theme_options['munich_shop_sidebar_header_image']
);

?>
<!-- <div id="cart" class="cart">
    <div id="cart-inner"></div>
</div> -->
<div class="title">
	<?php 
    if ( $shop_sidebar_options->header_type == 'text' ) {
    ?>
    <div class="text">
        <img src="<?php echo esc_attr( $shop_sidebar_options->header_image['url'] ); ?>" />
        <h4><?php echo esc_html( $shop_sidebar_options->header_title ); ?></h4>
        <em><?php echo esc_html( $shop_sidebar_options->header_subtitle ); ?></em>
    </div>
    <?php } elseif (isset($shop_sidebar_options->header_image)) { ?>
    <div class="image">
        <img src="<?php echo esc_attr( $shop_sidebar_options->header_image['url'] ); ?>" />
    </div>
    <?php } ?>
</div>
<div class="widgets">
    <div class="widget">
        <ul>
            <li>
                <a href="/press-release">All Press Release</a>
            </li>
            <?php
                wp_get_archives('cat=13'); 
            ?>
        </ul>
    </div>
</div>