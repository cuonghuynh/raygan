<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<?php global $theme_options; ?>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta name="format-detection" content="telephone=no" />
		<?php 		
		if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)){ ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<?php } ?>
		<title><?php wp_title(" - "); ?></title>
        <?php 
        $favicon = get_template_directory_uri() . '/assets/images/default/default-favicon.ico';
        if(isset($theme_options['opt-site-favicon'])) { $favicon = $theme_options['opt-site-favicon']['url']; }
        ?>
		<link rel="icon" href="<?php echo esc_url($favicon); ?>" type="image/x-icon" />
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
        <?php 
        munich_get_vc_custom_shortcode_styles();
        if(isset($theme_options['munich-analytics-code'])) { echo balanceTags($theme_options['munich-analytics-code']); }
		wp_head(); ?>
	</head>
	<?php 
    if(isset($theme_options['munich_featured_works'])) {
        $featuredWorks = $theme_options['munich_featured_works']; 
    }

	$featuredWorksCnt = 0;
	if(isset($featuredWorks) && is_array($featuredWorks) && sizeof($featuredWorks) > 0 ){ 
		foreach($featuredWorks as $workid => $value){ if(intval($value) == 1) $featuredWorksCnt++; }
	}
	$hasFeaturedWorks = $featuredWorksCnt > 0;
	?>
	<body <?php body_class("loading"); ?>>
		<div class="modal">
			<div class="panel">
				<p><span class="close">X</span></p>
				<div class="intro-image">
					<?php 
						$logo = get_template_directory_uri() . '/assets/images/default/logo-grey.png';  
						$fb_icon = get_template_directory_uri() . '/assets/images/default/facebook-icon.png';
						$artistic_guild = get_template_directory_uri() . '/assets/images/default/Artistic-guild-logo.jpg';
					?>
					<img src="<?php echo $logo; ?>" alt="">
					<div style="text-transform: uppercase" class="post-info">
						<p>"Like" our facebook: "Ray gan photography" for lastest updates<img style="height: 30px; margin-left: 10px;" src="<?php echo $fb_icon; ?>"></p><br>
						<p class="title"></p>
						<p>May to sept 2012: Bali, Hokkaido, paris, venice, santorini, new zealand</p>
						<p>Dec 2012: Grague</p><br>
						<p>Orthe packages include actual day wedding photography</p><br>
					</div>
				</div>
				<div class="footer">
					<p><span>www.rayganphotography.com</span><span>email: contact@rayganphotography.com</span><span>contact: +65 9437 8769</span><img src="<?php echo $artistic_guild; ?>" alt=""></p>
				</div>
			</div>
		</div>
        <div id="loading-page">
            <div class="spinner">
                <div class="spinner-container container1">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                </div>
                <div class="spinner-container container2">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                </div>
                <div class="spinner-container container3">
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                </div>
            </div>
        </div>
        <div id="site-body">
			<header id="site-header">
				<div class="bar">
                    <?php if(isset($theme_options['opt-site-logo']) && isset($theme_options['opt-site-logo'][ 'height' ])){ ?>
					<a href="<?php echo home_url(); ?>">
                        <?php $logo_height = is_array($theme_options['opt-site-logo']) ? intval( $theme_options['opt-site-logo'][ 'height' ] ) : 0;
                        if( isset($theme_options['opt-site-logo-retina']) && $theme_options['opt-site-logo-retina'] == 1 ) { $logo_height = $logo_height / 2; }
                        ?>
						<img src="<?php echo esc_attr( $theme_options['opt-site-logo']['url'] ); ?>" style="height: <?php echo esc_attr( $logo_height ); ?>px; width: auto;" alt="Site Logo" />
					</a>
                    <?php } ?>
					<?php if(  $hasFeaturedWorks ) { ?>
					<button id="toggle-featured-projects" class="icon-icon-star secondary-color"></button>
					<?php } ?>
					<button id="toggle-main-nav" class="toggle-button">
						<span class="bar first"></span>
						<span class="bar middle"></span>
						<span class="bar last"></span>
					</button>
				</div>

				<?php  if(  $hasFeaturedWorks ) { ?>
				<section id="featured-projects">
					<ul>
					<?php foreach($featuredWorks as $workid => $value){ 
						if(intval($value) == 1) { 
							$terms = get_the_terms( $workid, 'work_type' ); ?>
						<li>
							<a href="<?php echo get_permalink( $workid ); ?>"></a>
							<div class="content">
								<div class="inner" style="top: 50%;">
									<?php if( !empty( $terms ) ){ ?>
									<?php 	foreach( $terms as $term ) { ?>
										<?php echo '<a href="' . get_term_link( $term->slug, 'work_type' ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'munich' ), $term->name ) ) . '">' . $term->name . '</a>'; ?>
									<?php 	} ?>
									<?php } ?>
									<h6><?php echo get_the_title( $workid ); ?></h6>
								</div>
							</div>
							<div class="image preload" data-image="<?php echo wp_get_attachment_medium_url( get_post_thumbnail_id( $workid ) ); ?>"></div>
						</li>
				<?php   }
					}
				} ?>
					</ul>
				</section>

				<nav id="main-nav" data-back-button-caption="<?php echo _e('BACK', 'munich'); ?>">
					<div class="wrapper">
						<div class="inner">
							<a href="<?php echo home_url(); ?>">
								<img src="<?php echo esc_attr( $theme_options['opt-site-logo']['url'] ); ?>" <?php if(isset($logo_height)) { ?>style="height: <?php echo esc_attr( $logo_height ); ?>px; width: auto;" <?php } ?> alt="Site Logo" />
							</a>
							<div class="nav-text">
								<?php
                                    global $munich_allowed_html_tags;
                                    echo wp_kses( $theme_options['opt-nav-text'], $munich_allowed_html_tags ); ?>	
							</div>

							<?php wp_nav_menu(); ?>

						</div>
					</div>
					<?php get_template_part( 'partials/modules/social_networks_links' ); ?>
					<div class="subscribe" >
						<p style="font-size: 12px; font-family: 'Roboto Condensed', sans-serif; font-style: normal; margin: 0; color: #c4c4c4;">Subscribe to our Email Newsletter</p>
						<input type="text" name="email" id="email" placeholder="Email" style=" font-family: 'Roboto Condensed', sans-serif; border: none; background: none; border-bottom: 1px solid; width: 100%;">
						<input type="button" name="submit" value="SUBMIT" style="float: right; margin-top: 5px; border: none; background: none; background-color: #c93535; padding: 2px 10px; color: #fff; border-radius: 15px; font-size: 11px; font-weight: bold;" >
					<?php if( $hasFeaturedWorks ) { ?>
					<span id="featured-works-label" class="secondary-color"><span><?php _e("FEATURED PROJECTS", "munich"); ?></span></span>
					<?php } ?>
				</nav>
			</header>
            <div id="page-wrapper">
                <div id="page-inner">
