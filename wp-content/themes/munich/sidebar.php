<?php 

global $theme_options;
$blog_sidebar_options = (object) array(
    'sidebar_visibility'	=> $theme_options['munich_blog_sidebar_visibility'],
    'header_type'			=> $theme_options['munich_blog_sidebar_header'],
    'header_icon'			=> $theme_options['munich_blog_sidebar_header_icon'],
    'header_title'			=> $theme_options['munich_blog_sidebar_header_title'],
    'header_subtitle'		=> $theme_options['munich_blog_sidebar_header_subtitle']
);
if(isset($theme_options['munich_blog_sidebar_header_image'])) {
    $blog_sidebar_options->header_image = $theme_options['munich_blog_sidebar_header_image'];
}
?>

<div class="title">
	<?php 
    if ( $blog_sidebar_options->header_type == 'text' ) {
    ?>
    <div class="text">
        <img src="<?php echo esc_attr( $blog_sidebar_options->header_image['url'] ); ?>" /> <!-- new -->
        <h4><?php echo esc_html( $blog_sidebar_options->header_title ); ?></h4>
        <em><?php echo esc_html( $blog_sidebar_options->header_subtitle ); ?></em>
    </div>
    <?php } elseif(isset($blog_sidebar_options->header_image)) { ?>
    <div class="image">
        <img src="<?php echo esc_attr( $blog_sidebar_options->header_image['url'] ); ?>" />
    </div>
    <?php } ?>
</div>
    
<div class="widgets">
    <div class="widget">
        <ul>
            <li>
                <a href="/news-announcement">All News</a>
            </li>
            <?php
                wp_get_archives('cat=12'); 
            ?>
        </ul>
    </div>
</div>