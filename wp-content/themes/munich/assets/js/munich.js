/*jshint onevar: true, browser: true, white: true */
/*globals parseUri */

var MunichTheme,
    MutationObserver,
    jQuery;

(function ($) {
    "use strict";
    MunichTheme = {
        options: {
            elements: {
                body: '#page-body',                        // The site's page body element
                header: '#site-header',                    // The site's main header element
                mainMenu: '#main-nav',                    // The site's main menu element
                featuredWorks: '#featured-projects'     // The site's featured works menu element.
            },
            settings: {
                clickEvent: 'click',                    // The event to be fired when an element is pressed.
                clickEventEnd: 'click',                    // The event to be fired when an element is pressed.
                hoverStart: 'mouseenter',                // The event to be fired when an element is pressed.
                hoverEnd: 'mouseleave'                    // The event to be fired when an element is pressed.
            }
        },
        pages: {
            homepage: {
                options: {
                    elements: {
                        mainSlider: '#main-slider-desktop', // The homepage main slider for desktop
                        mobileSlider: '#main-slider-mobile' // mobile slider for mobile size
                    }
                },
                init: function () {
                    this.bindUIActions();
                },
                bindUIActions: function () {
                    var self = this;
                    $(window).ready(function () {
                        // Initialize the main slider
                        self.Modules.Slider.init(self);
                        // Position the logo correctly
                        $('#homepage-logo').positionWithinParent();
                        // Preload images
                        $('.preload').preloadImages();
                        $('li.slide:first-child').preloadImages({
                            onFinishLoad: function () {
                                MunichTheme.removeLoadingScreen();
                            }
                        });
                    });
                },
                Modules: {
                    Slider: {
                        init: function (self) {
                            var elements = self.options.elements;
                            
                            function showLogoOnFirstSlideOnly(slider) {
                                if ($('#homepage-logo.first-slide-only').length) {
                                    if (slider.animatingTo === 0) {
                                        $('#homepage-logo').addClass('visible');
                                    } else {
                                        $('#homepage-logo').removeClass('visible');
                                    }
                                }
                            }
                            
                            // Create the gallery
                            $(elements.mainSlider).createGallery({
                                beforeFunction: showLogoOnFirstSlideOnly,
                                distractionFree: $(elements.mainSlider).hasClass('distraction-free'),
                                logo: $('#homepage-logo'),
                                sliderOptions: {
                                    slideshow: false
                                }
                            });
                            $(elements.mobileSlider).createGallery({
                                beforeFunction: showLogoOnFirstSlideOnly,
                                distractionFree: $(elements.mobileSlider).hasClass('distraction-free'),
                                logo: $('#homepage-logo'),
                                sliderOptions: {
                                    slideshow: false
                                }
                            });
                            // Set the background images for the cloned slides.
                            $('.preload.clone').preloadImages();
                        }
                    }
                }
            },
            portfolio: {
                options: {
                    elements: {
                        portfolioList: $('#portfolio-list'),
                        tmpPortfolioList: $('#page-tmp'),
                        pageCounter: $('#current-page')
                    },
                    itemsPerPage: 8
                },
                init: function () {
                    // Initialize the main slider
                    this.Modules.PortfolioList.init(this);
                    // Init portfolio navigation
                    this.Modules.SetUpNavigation();
                },
                Modules: {
                    PortfolioList: {
                        init: function (self) {
                            var elements = self.options.elements;

                            // Set up the pagination (split the temporary grid into pages)
                            elements.tmpPortfolioList.createPostsGalleryPages({
                                itemsPerPage: self.options.itemsPerPage,
                                pageCounter: elements.pageCounter,
                                sufix: 'portfolio',
                                list: elements.portfolioList
                            });

                            // Build the gallery
                            $("#page-body.portfolio > .flexslider").createPostsGallery();
                        }
                    },
                    SetUpNavigation: function () {
                        // Set the portfolio-page-url value
                        $('input[name="portfolio-page-url"]').attr('value', window.location.href);
                        // Submit the forms
                        $('.portfolio-item form > a').off(MunichTheme.options.settings.clickEvent);
                        $('.portfolio-item form > a').on(MunichTheme.options.settings.clickEvent, function () {
                            $(this).parent().submit();
                            return false;
                        });
                    }
                }
            },
            portfolioSingle : {
                options: {
                    elements: {
                        mainSlider: $('#main-slider-desktop'),
                        relatedWorks: $('#related-works'),
                        bottomBar: {
                            bar: $('#work-bottom-bar'),
                            button: $('#toggle-work-bottom-bar')
                        }
                    },
                    elementsSelectors: {
                        mainSlider: '#main-slider-desktop',
                        relatedWorks: '#related-works',
                        bottomBar: {
                            bar: '#work-bottom-bar',
                            button: '#toggle-work-bottom-bar'
                        }
                    }
                },
                initOptions: function () {
                    var selectors = this.options.elementsSelectors,
                        elements = this.options.elements;
                    elements.mainSlider = $(selectors.mainSlider);
                    elements.relatedWorks = $(selectors.relatedWorks);
                    elements.bottomBar.bar = $(selectors.bottomBar.bar);
                    elements.bottomBar.button = $(selectors.bottomBar.button);
                },
                init: function () {
                    // Set body class if accessing through ajax navigation
                    if (!$("body").hasClass("single-portfolio")) {
                        $('body').attr('class', 'single single-portfolio');
                    }
                    // Init options
                    this.initOptions();
                    // Initialize the main slider
                    this.Modules.Slider.init(this);
                    // Initialize the bottom bar
                    this.Modules.BottomBar.init(this);
                    // Initialize the related works
                    this.Modules.RelatedWorks.init(this);
                    // Preload images
                    $('.preload').preloadImages();
                },
                Modules: {
                    Slider: {
                        init: function (self) {
                            var elements = self.options.elements;
                            // Create the gallery
                            elements.mainSlider.createGallery({
                                distractionFree: elements.mainSlider.hasClass('distraction-free'),
                                sliderOptions: {
                                    slideshow: false
                                }
                            });
                            // Set the background images for the cloned slides.
                            $('.preload.clone').preloadImages();
                        }
                    },
                    BottomBar: {
                        init: function (self) {
                            var elements = self.options.elements,
                                bottomBar = this,
                                textHeight;
                            elements.bottomBar.button.toggleButton({
                                toggle: { elements: elements.bottomBar.bar },
                                whenToggleOn: bottomBar.showBar,
                                whenToggleOff: bottomBar.hideBar
                            });
                            bottomBar.descriptionArea.init();

                            if ($(window).width() >= 1024) {
                                textHeight = 135;
                                if ($('#work-description').length && $('#work-description').outerHeight() > 135) {
                                    textHeight = $('#work-description').outerHeight();
                                }

                                elements.relatedWorks.css('height', textHeight + 'px');
                                elements.relatedWorks.find('img').css('height', (textHeight - 30) + 'px');
                            } else {
                                elements.relatedWorks.css('height', 'auto');
                            }
                            
                        },
                        showBar: function () {
                            var elements = MunichTheme.pages.portfolioSingle.options.elements;
                            
                            // Show the bottom bar
                            elements.bottomBar.bar.attr('style', '-webkit-transform: translate3d(0, -' + elements.bottomBar.bar.outerHeight() + 'px, 0); transform: translate3d(0, -' + elements.bottomBar.bar.outerHeight() + 'px, 0); -ms-transform: translate(0, -' + elements.bottomBar.bar.outerHeight() + 'px)');
                            elements.mainSlider.attr('style', '-webkit-transform: translate3d(0, -' + elements.bottomBar.bar.outerHeight() + 'px, 0); transform: translate3d(0, -' + elements.bottomBar.bar.outerHeight() + 'px, 0); -ms-transform: translate(0, -' + elements.bottomBar.bar.outerHeight() + 'px)');
                        },
                        hideBar: function () {
                            var elements = MunichTheme.pages.portfolioSingle.options.elements;
                            elements.bottomBar.bar.attr('style', '-webkit-transform: translate3d(0, 0, 0); transform: translate3d(0, 0, 0); -ms-transform: translate(0, 0)');
                            elements.mainSlider.attr('style', '-webkit-transform: translate3d(0, 0, 0); transform: translate3d(0, 0, 0); -ms-transform: translate(0, 0)');

                        },
                        descriptionArea: {
                            init: function () {
                                var words = $('#work-description').text().split(' ').length;
                                if (words <= 20) {
                                    $('#work-description').css('width', '25%');
                                    $('#related-works').css('width', '75%');
                                } else {
                                    $('#work-description').css('width', words + '%');
                                    if (words <= 50) {
                                        $('#related-works').css('width', (100 - words) + '%');
                                    }
                                }
                            }
                        }
                    },
                    RelatedWorks: {
                        init: function (self) {
                            var relatedWorks = self.options.elements.relatedWorks;
                            // Init the kinetic gallery
                            relatedWorks.find('.wrapper').kinetic();
                            // Handle the navigation buttons behaviour
                            this.navigation.init(relatedWorks);
                        },
                        navigation: {
                            init: function (relatedWorks) {
                                // Next
                                relatedWorks.find("button.next").live(MunichTheme.options.settings.hoverStart, function () {
                                    relatedWorks.find('.wrapper').kinetic('start', { velocity: 3 });
                                }).live(MunichTheme.options.settings.hoverEnd, function () {
                                    relatedWorks.find('.wrapper').kinetic('end');
                                });
                                // Previous
                                relatedWorks.find("button.previous").live(MunichTheme.options.settings.hoverStart, function () {
                                    relatedWorks.find('.wrapper').kinetic('start', { velocity: -3 });
                                }).live(MunichTheme.options.settings.hoverEnd, function () {
                                    relatedWorks.find('.wrapper').kinetic('end');
                                });
                            }
                        }
                    }
                }
            },
            blog: {
                options: {
                    elements: {
                        posts: $('.post'),
                        sidebar: $('.sidebar')
                    }
                },
                init: function () {
                    this.bindUIActions();
                },
                bindUIActions: function () {
                    var self = this;
                    $(window).ready(function () {
                        // Initialize the posts
                        self.Modules.Posts.init();
                        // Create the gallery
                        $('#page-body > .flexslider').createPostsGallery();
                        // Initialize the sidebar
                        self.Modules.Sidebar.init(self);
                    });
                },
                Modules: {
                    Posts: {
                        init: function () {
                            function adjustPostsElements() {
                                if ($(window).width() < 1024) {
                                    $('.blog-posts .post .info, .blog-posts .post .image').attr('style', '');
                                    $('.blog-posts .post .image').preloadImages();
                                } else {
                                    var info_elements = $('.blog-posts .post .info'), image_elements = $('.blog-posts .post .image'),
                                        list_height = $('.blog-posts').height(),
                                        top_content_height = 0;

                                    info_elements.css('height', 'auto');

                                    info_elements.each(function () {
                                        if ($(this).outerHeight() > top_content_height) {
                                            top_content_height = $(this).outerHeight();
                                        }
                                    });

                                    info_elements.css('height', top_content_height);
                                    image_elements.css('height', list_height - top_content_height);
                                }
                            }
                            
                            $(window).resize(adjustPostsElements);
                            adjustPostsElements();
                        }
                    },
                    Sidebar: {
                        init: function (self) {
                            // Init the toggle button for the main menu
                            $('#toggle-sidebar').toggleButton({
                                toggle: { elements: $(self.options.elements.sidebar) },
                                replicate: [{
                                    elements: $('body, #toggle-sidebar'),
                                    classes: 'open open-sidebar'
                                }]
                            });
                        }
                    }
                }
            },
            blogSingle: {
                options: {
                    elements: {
                        post: $('article.post'),
                        mainSlider: $('#main-slider'),
                        sidebar: $('.sidebar'),
                        comments: $('section.comments'),
                        content: $('article.post .content')
                    }
                },
                init: function () {
                    this.bindUIActions();
                },
                bindUIActions: function () {
                    var self = this;
                    $(window).ready(function () {
                        // Initialize the slider
                        self.Modules.Slider.init(self);
                        // Initialize the comments section
                        self.Modules.Comments.init(self);
                        // Initialize the sidebar
                        MunichTheme.pages.blog.Modules.Sidebar.init(self);
                    });
                },
                Modules: {
                    Slider: {
                        init: function (self) {
                            var elements = self.options.elements;
                            // Create the gallery
                            elements.mainSlider.createGallery({
                                distractionFree: $(elements.mainSlider).hasClass('distraction-free'),
                                sliderOptions: {
                                    slideshow: false
                                }
                            });
                            // Set the background images for the cloned slides.
                            $('.preload.clone').preloadImages();
                        }
                    },
                    Comments: {
                        init: function (self) {
                            var elements = self.options.elements;
                            // Textarea autosize
                            $('textarea').autosize();
                            // Move comments section to the bottom
                            elements.comments.fillSpaceUntilBottom();
                        }
                    }
                }
            },
            gallery: {
                options: {
                    elements: {
                        mainSlider: $('#main-slider-desktop'),
                        thumbs: $('.munich-gallery-thumbs')
                    }
                },
                init: function () {
                    this.bindUIActions(this);
                },
                bindUIActions: function (self) {
                    $(document).ready(function () {
                        // Init the slider
                        self.Modules.Slider.init(self);
                        // Init the thumb area
                        self.Modules.Thumbs.init(self);
                    });
                },
                Modules: {
                    Slider: {
                        init: function (self) {
                            var elements = self.options.elements;
                            // Create the gallery
                            elements.mainSlider.createGallery({
                                distractionFree: $(elements.mainSlider).hasClass('distraction-free'),
                                sliderOptions: {
                                    slideshow: true
                                }
                            });
                        }
                    },
                    Thumbs: {
                        init: function (self) {
                            $('#page-body').addClass('thumbs-visible');
                            setTimeout(function () {
                                if (!$('.munich-gallery-thumbs').is(':hover')) {
                                    $('#page-body').removeClass('thumbs-visible');
                                }
                            }, 2000);
                            self.options.elements.thumbs.scrollableOnHover({
                                touchDevice: window.touch_device
                            });
                        }
                    }
                }
            },
            shop: {
                options: {
                    elements: {
                        shopList: $('#shop-list'),
                        tmpPortfolioList: $('#page-tmp'),
                        pageCounter: $('#current-page'),
                        toggleCartButtonSelector: "#cart-toggle-wrapper",
                        cartSelector: "#cart"
                    },
                    itemsPerPage: 8
                },
                init: function () {
                    this.Modules.ShopList.init(this);
                    this.Modules.Sidebar.init(this);
                },
                Modules: {
                    ShopList: {
                        init: function (self) {
                            var elements = self.options.elements;

                            // Set up the pagination (split the temporary grid into pages)
                            elements.tmpPortfolioList.createPostsGalleryPages({
                                itemsPerPage: self.options.itemsPerPage,
                                pageCounter: elements.pageCounter,
                                sufix: 'shop',
                                list: elements.shopList
                            });

                            // Build the gallery
                            $("#page-body.shop > .flexslider").createPostsGallery();
                            
                        }
                    },
                    Sidebar: {
                        init: function () {
                            function initSidebarCart() {
                                var elements = MunichTheme.pages.shop.options.elements;
                                // Toggle the cart contents
                                $(elements.toggleCartButtonSelector).toggleButton({
                                    toggle: {
                                        elements: $(elements.cartSelector),
                                        classes: 'open'
                                    },
                                    unless: function () { return $(elements.toggleCartButtonSelector).hasClass('empty-cart'); }
                                });
                            }
                            
                            if (window.MutationObserver) {
                                var elements = MunichTheme.pages.shop.options.elements,
                                    target = document.querySelector(elements.cartSelector),
                                    observer = new MutationObserver(function (mutations) {
                                        mutations.forEach(function () {
                                            initSidebarCart();
                                        });
                                    }),
                                    config = { attributes: true, childList: true, characterData: true };

                                // Observe the cart element and look for changes.
                                if (target) {
                                    observer.observe(target, config);
                                }
                            }
                            
                            
                        }
                    }
                }
            },
            shopSingle: {
                options: {
                    elements: {
                        reviews: $("#reviews"),
                        toggleReviewsButton: $("#toggle-reviews"),
                        sidebar: $('.sidebar')
                    }
                },
                init: function () {
                    // Initialize the Reviews module
                    this.Modules.Reviews.init();
                    // Initialize the sidebar
                    MunichTheme.pages.blog.Modules.Sidebar.init(this);
                    MunichTheme.pages.shop.Modules.Sidebar.init(this);
                },
                Modules: {
                    Reviews: {
                        init: function () {
                            $('textarea').autosize();
                            $('#tab-reviews').fillSpaceUntilBottom({
                                elementWrapper: $('div.summary'),
                                elementsOnTop: $('#summary-head, #tab-description')
                            });
                        }
                    }
                }
            },
            contact: {
                init: function () {
                    this.Modules.ContactForm.init(this);
                    this.Modules.Map.init(this);
                },
                options: {
                    elements: {
                        contactForm: $('section.contact-form'),
                        map: {
                            toggleButtonDesktop: $('#toggle-map-desktop'),
                            toggleButtonMobile: $('#toggle-map-mobile'),
                            mapCanvasDesktop: $('#contact-map-desktop'),
                            mapCanvasMobile: $('#contact-map-mobile')
                        }
                    }
                },
                Modules: {
                    ContactForm: {
                        init: function (self) {
                            var elements = self.options.elements;
                            elements.contactForm.fillSpaceUntilBottom({
                                elementWrapper: $('section.content'),
                                elementsOnTop: $('section.content .inner')
                            });
                            $('textarea').autosize();
                        }
                    },
                    Map: {
                        init: function (self) {
                            var elements = self.options.elements;
                            elements.map.toggleButtonDesktop.toggleButton({
                                toggle: { elements: elements.map.mapCanvasDesktop }
                            });
                            elements.map.toggleButtonMobile.toggleButton({
                                toggle: { elements: elements.map.mapCanvasMobile }
                            });
                        }
                    }
                }
            },
            customPage: {
                init: function () {
                    this.Modules.Elements.init();
                    // Init Visual Composer modules
                    if (typeof window.vc_js === "funciton") {
                        window.vc_js();
                    }
                },
                Modules: {
                    Elements: {
                        init: function () {
                            // Visual Composer progress bars
                            if ($('.vc_bar').length > 0) {
                                $('.vc_bar').each(function () {
                                    $(this).css('width', parseInt($(this).attr('data-percentage-value'), 10) + '%');
                                });
                            }
                        }
                    }
                }
            }
        },
        init: function () {
            if (document.location.hash) {
                document.location.hash = "";
            }
            this.setUpNavigation();
            this.setDefaultOptions();
            this.initCurrentPage();
        },
        setDefaultOptions: function () {
            var self = this;
            // Set the click event to TOUCHSTART if viewing the site on a touch-screen device.
            if (window.touch_device) {
                self.options.settings.clickEvent = 'touchstart';
                self.options.settings.hoverStart = 'touchstart';
                self.options.settings.hoverEnd = 'touchend';
                $('html').addClass('touch');
            } else {
                $('html').addClass('no-touch');
            }
        },
        setUpNavigation: function () {
            $("a:not(#featured-projects a, .portfolio-page form > a, .menu-item-has-children > a, a.woocommerce-main-image, .thumbnails > a, a.prettyphoto)").on(MunichTheme.options.settings.clickEvent, function () {
                return MunichTheme.navigateToPage($(this).attr('href'));
            });
        },
        initCurrentPage: function (fromAnotherPage, menu_open) {
            var self = this,
                options = MunichTheme.options,
                pageClass = ($('#page-body').length > 0) ? $('#page-body').attr('class') : "";
            
            /* Init Main Navigation & Featured Works */
            self.Modules.MainNavigation.init(); // Init the site's main navigation
            self.Modules.FeaturedWorks.init(); // Init the site's featured works
            self.Modules.Scrollbars.init(); // Init the site's featured works
            
            // Peload the page's images
            $('.preload').preloadImages();
            
            /* Set the main menu state to open if coming from another page */
            if (fromAnotherPage && menu_open) {
                $(options.elements.mainMenu + ', ' + options.elements.header + ', ' + options.elements.body + ', #toggle-main-nav, #site-body').addClass('open open-main-menu');
            }

            /* Init the corresponding page scripts */
            if (pageClass.indexOf('homepage') > -1) {
                // Homepage
                MunichTheme.pages.homepage.init();
            } else if (pageClass.indexOf('portfolio') > -1 && pageClass.indexOf('single-portfolio') < 0) {
                // Portfolio Archive
                MunichTheme.pages.portfolio.init();
            } else if (pageClass.indexOf('single-portfolio') > -1) {
                // Single Portfolio
                MunichTheme.pages.portfolioSingle.init();
            } else if (pageClass.indexOf('blog') > -1) {
                // Blog Archive
                MunichTheme.pages.blog.init();
            } else if (pageClass.indexOf('single-post') > -1) {
                // Blog Single Post
                MunichTheme.pages.blogSingle.init();
            } else if (pageClass.indexOf('gallery-page') > -1) {
                // Gallery page
                MunichTheme.pages.gallery.init();
            } else if (pageClass.indexOf('shop-archive') > -1) {
                // Shop archive page
                MunichTheme.pages.shop.init();
            } else if (pageClass.indexOf('single-product') > -1) {
                // Single product page
                MunichTheme.pages.shopSingle.init();
            } else if (pageClass.indexOf('contact') > -1) {
                // Contact page
                MunichTheme.pages.contact.init();
            } else if (pageClass.indexOf('custom-page') > -1) {
                // Single product page
                MunichTheme.pages.customPage.init();
            }

            if ($('#page-body').length > 0 && pageClass.indexOf('homepage') === -1) {
                MunichTheme.removeLoadingScreen();
            } else if ($('.post-password-form').length > 0) {
                $('body').addClass('password-protected');
                MunichTheme.removeLoadingScreen();
            }
        },
        navigateToPage: function (url) {
            var target_url = parseUri(url),
                current_url = parseUri(document.URL);

            if (target_url.host === current_url.host) {
                $('body').addClass('loading');
                $('.page-loader').addClass('active');
                setTimeout(function () {
                    location.href = url;
                }, 250);
                return false;
            }
            return true;
        },
        removeLoadingScreen: function () {
            setTimeout(function () {
                $('body').removeClass('loading');
                setTimeout(function () {
                    $('.page-loader').removeClass('active');
                }, 750);
            }, 250);
        },
        Modules: {
            MainNavigation: {
                /* 
                  Initializes the site's main menu behaviour.
                */
                init: function () {
                    var options = MunichTheme.options;

                    // Add the BACK buttons to the submenus
                    $('.sub-menu li.back').remove();
                    $('.sub-menu').prepend('<li class="back">' + $('#main-nav').data('back-button-caption') + '</li>');

                    // Enter a submenu
                    $('li.menu-item-has-children').live('click', function () {
                        $(this).addClass('open').parent().addClass('hidden');
                        setMenuWrapperHeight();
                    });

                    // Leave a submenu
                    $('li.back').live('click', function (e) {
                        e.stopPropagation();
                        $(this).parent().parent().removeClass('open').parent().removeClass("hidden");
                        setMenuWrapperHeight();
                    });

                    // Disable parent menus links
                    $('.menu-item-has-children > a').on('click', function (e) {
                        e.preventDefault();
                    });

                    // Init the toggle button for the main menu
                    $('#toggle-main-nav').toggleButton({
                        toggle: { elements: $(options.elements.mainMenu) },
                        remove: [{ elements: $(options.elements.featuredWorks + ', #toggle-featured-projects') }],
                        replicate: [{
                            elements: $(options.elements.header + ', ' + options.elements.body + ', #toggle-main-nav, #site-body'),
                            classes: 'open open-main-menu'
                        }]
                    });
                    
                    function setMenuWrapperHeight(){
                        var sub_menu_height = "auto";
                        if($("#main-nav li.open").length > 0){
                            sub_menu_height = $("#main-nav li.open > .sub-menu").last().height() + parseInt($("#main-nav .menu").css("margin-top"), 10);
                        }
                        $("#main-nav .inner > div:nth-child(3)").css('height', sub_menu_height);
                    }
                }
            },
            FeaturedWorks: {
                /* 
                  Initializes the featured works menu behaviour.
                */
                init: function () {
                    var options = MunichTheme.options,
                        elements = MunichTheme.options.elements;

                    // Init the toggle button for the featured works menu
                    $('#toggle-featured-projects, #featured-works-label').toggleButton({
                        toggle: {
                            elements: $(options.elements.featuredWorks)
                        },
                        remove: [
                            { elements: $(options.elements.mainMenu + ', #toggle-main-nav'), classes: 'open open-main-menu' },
                            { elements: $('#page-body, #site-header'), classes: 'open-main-menu' }
                        ],
                        replicate: [{
                            elements: $(options.elements.header + ', #toggle-featured-projects'),
                            classes: 'open open-featured-works'
                        }],
                        unless: function () {
                            return $(options.elements.featuredWorks).hasClass('open-by-hover');
                        }
                    });

                    // Add the toggle behaviour to the Featured Works
                    $('#toggle-featured-projects, #featured-works-label').on('mouseenter', function () {
                        if (!window.touch_device && jQuery('body').width() >= 1024) {
                            $('#site-body, #toggle-main-nav, ' + elements.body + ', ' + elements.header + ', ' + elements.mainMenu).removeClass('open open-main-menu');
                            $(elements.featuredWorks).addClass('open open-by-hover');
                        }
                    });
                    $(options.elements.featuredWorks).on('mouseleave', function () {
                        if (!window.touch_device && jQuery('body').width() >= 1024) {
                            if ($(this).hasClass('open-by-hover')) {
                                $(this).removeClass('open open-by-hover');
                            }
                        }
                    });
                    
                    // Center the items content
                    $(options.elements.featuredWorks).find('.inner').positionWithinParent();

                    // Init the custom scrollbar
                    if (!window.touch_device) {
                        $("#featured-projects").mCustomScrollbar({ theme: "minimal" });
                        $(window).resize(function () {
                            $("#featured-projects").mCustomScrollbar("update");
                        });
                    }
                    
                    this.setUpAjaxNavigation();

                },
                setUpAjaxNavigation: function () {
                    function loadPage(raw_url, title) {
                        $('#page-wrapper').load(raw_url + ' #page-inner', function () {
                            document.title = title;
                            MunichTheme.initCurrentPage(true, $('#site-header').hasClass('open'));
                            $("#featured-projects li").removeClass("loading");
                        });
                    }
                    
                    $('#featured-projects a').off("click");
                    $('#featured-projects a').on("click", function (e) {
                        e.preventDefault();
                        var raw_url = $(this).attr('href'),
                            title = $(this).parent().find("h6").text().trim().capitalize(),
                            target_url = parseUri(raw_url),
                            current_url = parseUri(document.URL);

                        if (target_url.host === current_url.host && $(window).width() >= 1024) {
                            $(this).parent().addClass('loading');
                            loadPage(raw_url, title);
                        } else {
                            location.href = raw_url;
                        }
                    });

                }
            },
            Scrollbars: {
                init: function () {
                    // Main Nav
                    if ($('#main-nav .wrapper').length > 0) {
                        $("#main-nav .wrapper").mCustomScrollbar({ theme: "minimal" });
                        $(window).resize(function () {
                            $("#main-nav .wrapper").mCustomScrollbar("update");
                        });
                    }

                    // Custom Pages
                    if ($('section.content').length > 0 && $('#page-body.contact').length === 0 && $('#page-body.custom-page').length > -1) {
                        $("section.content").mCustomScrollbar({ theme: "minimal-dark" });
                        $(window).resize(function () {
                            $("section.content").mCustomScrollbar("update");
                        });
                    }
                    
                    //Contact Page
                    if ($('#page-body.contact').length > 0) {
                        if ($(window).width() > 1024) {
                            $("section.content").mCustomScrollbar({ theme: "minimal-dark" });
                        }
                        $(window).resize(function () {
                            if ($(window).width() > 1024) {
                                $("section.content").mCustomScrollbar("update", true);
                            } else {
                                $("section.content").mCustomScrollbar("disable", true);
                            }
                        });
                    }
                    
                    // Blog Archive
                    if ($('#page-body.blog').length > 0 && $('.sidebar').length > 0) {
                        $(".sidebar .wrapper").mCustomScrollbar({ theme: "minimal" });
                        $(window).resize(function () {
                            $(".sidebar .wrapper").mCustomScrollbar("update");
                        });
                    }
                    
                    // Portfolio Archive
                    if ($('#page-body.portfolio').length > 0 && $('.sidebar').length > 0) {
                        $(".sidebar .wrapper").mCustomScrollbar({ theme: "minimal" });
                        $(window).resize(function () {
                            $(".sidebar .wrapper").mCustomScrollbar("update");
                        });
                    }
                    
                    // Blog Post
                    if ($('article.post').length > 0) {
                        $("article.post").mCustomScrollbar({ theme: "minimal-dark" });
                        $(window).resize(function () {
                            $("article.post").mCustomScrollbar("update");
                        });
                    }
                    
                    // Blog Post Sidebar
                    if ($('.single-post .sidebar').length > 0) {
                        $(".single-post .sidebar").mCustomScrollbar({ theme: "minimal" });
                        $(window).resize(function () {
                            $(".single-post .sidebar").mCustomScrollbar("update");
                        });
                    }
                    
                    // Shop Archive
                    if ($('#page-body.shop-archive').length > 0 && $('.sidebar').length > 0) {
                        $(".sidebar .wrapper").mCustomScrollbar({ theme: "minimal" });
                        $(window).resize(function () {
                            $(".sidebar .wrapper").mCustomScrollbar("update");
                        });
                    }
                    
                    // Shop Single
                    if ($('.woocommerce-page').length > 0) {
                        $(".woocommerce-page div.product > div").mCustomScrollbar({ theme: "minimal-dark" });
                        $(window).resize(function () {
                            $(".woocommerce-page div.product > div").mCustomScrollbar("update");
                        });
                    }
                }
            }
        }
    };
}(jQuery));

MunichTheme.init();