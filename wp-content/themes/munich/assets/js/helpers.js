/*jshint white: true*/
/*globals parseUri, jQuery, MunichTheme, swapNodes, $f, parseVideo, createVideoFrame, type */

/**************************/
/*    Helper Functions    */
/**************************/

// Check for mobile devices
if (window.touch_device === undefined) {
    if (navigator.userAgent.match(/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/)) {
        window.touch_device = true;
    } else {
        window.touch_device = false;
    }
}

(function ($) {
    "use strict";
	/* 
	  Used for positioning elments within its parent element so that:
	    * top: 0% positions the element's top at the very top of its parent.
	    * top: 100% positions the element's bottom at the very bottom of its parent.
	    * same for bottom, left and right attributes.
	  All the elements need to have the top, bottom, left or right attributes set inline.

	   - elementsSelector:string	the elements whose images will be loaded.
	*/

	var isMobile = window.matchMedia("only screen and (max-width: 768px)");

    if (!isMobile.matches) {
        $('.type-post').hover(function() {
			$(this).find('.post-header p').css('margin-top', '50px');
		}, function() {
			$(this).find('.post-header p').css('margin-top', '0');
		});

		$('.popup-title').click(function(event) {
			/* Act on the event */
			var img = $(this).data('image');
			var title = $(this).data('title');
			$('.modal').css('display', 'block');
			$('.modal .panel .intro-image').css('background-image', 'url(' + img + ')');
			$('.modal .panel .intro-image .title').html(title);
		});

		$('.modal .panel .close').click(function(event) {
			/* Act on the event */
			$('.modal').css('display', 'none');
			
		});
    } else {
    	$('.popup-title').click(function(event) {
    		/* Act on the event */
    		var url = $(this).data('url');
    		window.location = url
    	});
    	
    }

	jQuery.fn.positionWithinParent = function () {
		var elements = this, fromRight, fromLeft, fromTop, fromBottom;
		
		$(elements).each(function () {
			var el = $(this),
                center = function () {
                    var element = el,
                        parent = el.parent();
                    if (element[0].style.right.length) {
                        fromRight = parseInt(element[0].style.right, 10);
                        element.css('margin-right', element.width() * (fromRight / 100) * (-1));
                    } else if (element[0].style.left.length) {
                        fromLeft = parseInt(element[0].style.left, 10);
                        element.css('margin-left', element.width() * (fromLeft / 100) * (-1));
                    }

                    if (element[0].style.top.length) {
                        fromTop = parseInt(element[0].style.top, 10);
                        element.css('margin-top', element.height() * (fromTop / 100) * (-1));
                    } else if (element[0].style.bottom.length) {
                        fromBottom = parseInt(element[0].style.bottom, 10);
                        element.css('margin-bottom', element.height() * (fromBottom / 100) * (-1));
                    }
                };

			$(window).resize(center);
			$(window).on('orientationchange', center);

			center();
			setTimeout(center, 500);
		});
	};

	/* 
	 * Used for creating a posts gallery (blog post, portfolio item).
	 *
	 *  - minimumWindowWidth:int	the element to be initialised as a gallery.
	 *  - navigationButtons:object	the elements for the gallery navigation buttons { prev: element, next: element }
	 *  - pageCounter:int			the element which keeps count of the current/total pages of the gallery.
	 *  - beforeFunction:function	a function that will be triggered right before a slide animation begins
	*/
	jQuery.fn.createPostsGallery = function (options) {

		/* Default options values */
		var settings = {
			minimumWindowWidth: 1024,
            galleryWidthOffset: 280,
			navigationButtons: {
				prev: $('#prev-page'),
				next: $('#next-page')
			},
			pageCounter: $('#current-page'),
			beforeFunction: function () {}
		}, gallery = $(this);
        
		settings = jQuery.extend(true, settings, options);
        
        function updateGallery() {
			var galleryActive = gallery.find('.flex-viewport').length > 0;

			if (!galleryActive && $(window).width() >= settings.minimumWindowWidth) {
				gallery.flexslider({
					animation: "slide",
					selector: ".slides > ul",
					mousewheel: false,
                    start: function (slider) {
                        //custom mousewheel:
                        var timer = null,
                            wheeling = false;
                        gallery.on('mousewheel', function (event, delta, deltaX, deltaY) {
                            if (timer) {
                                clearTimeout(timer);
                            }
                            if (!wheeling) {
                                var target = delta < 0 ? slider.getTarget('next') : slider.getTarget('prev');
                                slider.flexAnimate(target, true);
                            }
                            wheeling = true;
                            timer = setTimeout(function () {
                                wheeling = false;
                            }, 60);
                        });
                    },
					animationLoop: false,
					before: function (slider) {
                        var navigatingForward = true,
                            currentPage = slider.currentSlide + 1,
                            animatingToPage = slider.animatingTo + 1,
                            nextPage = animatingToPage + 1;
                        // Get the navigation direction
                        if (slider.animatingTo < slider.currentSlide) {
                            navigatingForward = false;
                        }
                        // Update the pagination indicators.
						settings.pageCounter.html('<span>' + animatingToPage + '</span><span>/</span><span>' + settings.pageCounter.attr('data-pages') + '</span>');
						gallery.find('#page-' + animatingToPage).addClass('visible');
						gallery.find('#page-' + (animatingToPage - 1)).addClass('not-last-page');
                        // See which is going to be the next page depending on the navigation direction
                        if (!navigatingForward) {
                            nextPage = animatingToPage - 1;
                        }
                        // Make sure the slide we are animating to always loads its images
                        gallery.find('#page-' + animatingToPage + ' .image').preloadImages();
                        // Preload the next slide images
						if (gallery.find('#page-' + nextPage).length > 0) {
                            gallery.find('#page-' + nextPage + ' .image').preloadImages();
                        }
                        // Call the beforeFunction from the options.
						settings.beforeFunction();
					},
					slideshow: false
                });
				galleryActive = true;
			} else if (galleryActive && $(window).width() < settings.minimumWindowWidth) {
				gallery.flexslider('destroy');
				galleryActive = false;
			}
		}

		$(window).resize(function () {
            updateGallery();
        });
        updateGallery();

		// Load the first page images, and the second page images after that

		gallery.find('#page-1').find('.image').preloadImages({
			onFinishLoad: function () {
				// Load the next page's images when all images in this page are loaded.
				if (gallery.find('#page-2').length > 0) {
					gallery.find('#page-2 .image').preloadImages();
                }
			}
		});

		/*  Gallery navigation */

		// Desktop
		settings.navigationButtons.prev.on(MunichTheme.options.settings.clickEvent, function () {
			gallery.flexslider("prev");
		});
		settings.navigationButtons.next.on(MunichTheme.options.settings.clickEvent, function () {
			gallery.flexslider("next");
		});

		// Mobile
		// Add the LOAD MORE buttons to each page except the last one.
		gallery.find('.page').each(function () {
			var current_page = $(this);
			if (current_page.find('.load-more').length === 0 && current_page.index() !== $('.page').last().index()) {
				$('<button class="load-more">LOAD MORE</button>').appendTo(current_page);
			}
		});

		gallery.find('button.load-more').on(MunichTheme.options.settings.clickEvent, function () {
			var navigatingTo = $(this).parent().next();
			navigatingTo.addClass('visible');
			$(this).parent().addClass('not-last-page');
			$(this).css('display', 'none');
			if (navigatingTo.next().length > 0) {
				navigatingTo.next().find('.image').preloadImages();
            }
		});
        
        // Load the second page if we are on a portrait tablet
        if ($(window).width() >= 768 && $(window).width() < 1024) {
            gallery.find('button.load-more').first().trigger(MunichTheme.options.settings.clickEvent);
        }
	};

	/* 
	 * Takes the temporary grid which contains a single UL with all the portfolio items and splits it into pages.
	 *
	 *  - itemsPerPage:int		the amount of items per page.
	 *  - pageCounter:object	the element that displays the page count.
	 *  - sufix:string			the sufix
	 *  - list:object			the list object where the pages are located.
	*/
	jQuery.fn.createPostsGalleryPages = function (options) {
        
		/* Default options values */
		var settings = {
			itemsPerPage: 8,
			pageCounter: $("#current-page"),
			sufix: 'portfolio',
			list: $('#portfolio-list')
		},
            tmpPage = $(this),
            pages = Math.ceil((tmpPage.find('li.wide').length * 2 + tmpPage.find('li.portrait').length) / settings.itemsPerPage),
            html = "<div>",
            p = 1, count, current_val, visible, page, sum, grid;
		settings = jQuery.extend(true, settings, options);
        
		// Calculate the amount of pages
		settings.pageCounter.attr('data-pages', pages).html('<span>1</span><span>/</span><span>' + pages + '</span>');
		if (pages <= 1) {
			settings.pageCounter.parent().addClass('no-pages');
			$('body').addClass('one-page-' + settings.sufix);
		}
        
		// Split the temprary grid into N pages
		for (p = 1; p <= pages; p += 1) {
			count = 0;
            current_val = 0;
			visible = "";
			if (p === 1) {
                visible = " visible ";
            }
			html += '<ul id="page-' + p + '" class="page ' + settings.sufix + '-page' + visible + '">';
			tmpPage.find('li.' + settings.sufix + '-item').each(function () {
				var item = $(this);
				current_val = item.hasClass('wide') ? 2 : 1;

				if (count + current_val < settings.itemsPerPage) {
					// There's room for this and more items, no problem.
					html += $('<div>').append(item.clone()).remove().html();
					count += current_val;
					item.remove();
				} else if (count + current_val === settings.itemsPerPage) {
					// There's room for this item and the page is full
					html += $('<div>').append(item.clone()).remove().html();
					count = 0;
					item.remove();
					return false; // Move on to the next page
				}
			});
			html += '</ul>';
		}
		html += '</div>';

        function reArrange(item) {
			// Find the following 1x1 block
			var switchItem = $(item.nextAll('.portrait'));
			if (!switchItem.length) { switchItem = $(item.prevAll('.portrait')); }
			// Swap the items
			if (switchItem.length) { swapNodes(item[0], switchItem[0]); }
		}
            
		// Arrange the items inside each page so that there are 4 spaces occupied per row.
		grid = $(html);
		grid.find('.' + settings.sufix + '-page').each(function () {
			page = $(this);
            sum = 0;
			page.find('.' + settings.sufix + '-item').each(function () {
				var item = $(this);
				sum = item.hasClass('wide') ? sum + 2 : sum + 1;
				if (sum >= 4) {
					if (sum === 5) { reArrange(item); }
					return false;
				}
			});
		});

		// Remove the temporary grid
		tmpPage.remove();

		// Add the new grid
		settings.list.append(grid.find('.' + settings.sufix + '-page'));

		// Detect the position of the items
		setTimeout(function () {
			$('.posts-gallery li.item').removeClass('top left right bottom');
			$('.posts-gallery li.item').each(function () {
				var top = Math.floor($(this).position().top),
					left = Math.floor($(this).position().left),
					bottom = Math.floor($(this).position().top + $(this).outerHeight()),
					right =	Math.floor($(this).position().left + $(this).outerWidth());

				if (top === 0) { $(this).addClass('top'); }
				if (left === 0) { $(this).addClass('left'); }
				if (bottom === $(this).parent().height() || (bottom + 1) === $(this).parent().height() ||  (bottom - 1) === $(this).parent().height()) { $(this).addClass('bottom'); }
				if (right === $(this).parent().width() || (right + 1) === $(this).parent().width() || (right - 1) === $(this).parent().width()) { $(this).addClass('right'); }
			});
		}, 500);

		// Focus items on touchstart
		if (window.touch_device) {
			$('.' + settings.sufix + '-item').on('touchstart', function () {
				$('.' + settings.sufix + '-item').removeClass('focused');
				$(this).addClass('focused');
			});
		}
	};

	/* 
	 * Used for creating a gallery.
	 *
	 *  - elementSelector:string	the element to be initialised as a gallery.
	 *  - sliderOptions:object		the options for intialising the flexslider object
	 *  - beforeFunction:function	a function that will be triggered right before a slide animation begins
	*/
	jQuery.fn.createGallery = function (options) {
		/* Default options values */
		var sliderElement = this,
            settings = {
                distractionFree: true,
                distractingFreeClass: 'distraction-free',
                distractingElements: $('#site-header, .munich-gallery .slide > .description, .munich-gallery .overlay, #homepage-logo'),
                sliderOptions: {
                    animation: 'fade',
                    directionNav: false,
                    mousewheel: false,
                    start: function (slider) {
                        //custom mousewheel:
                        var timer = null,
                            wheeling = false;
                        sliderElement.on('mousewheel', function (event, delta, deltaX, deltaY) {
                            if (timer) {
                                clearTimeout(timer);
                            }
                            if (!wheeling) {
                                var target = delta < 0 ? slider.getTarget('next') : slider.getTarget('prev');
                                slider.flexAnimate(target, true);
                            }
                            wheeling = true;
                            timer = setTimeout(function () {
                                wheeling = false;
                            }, 60);
                        });
                    },
                    animationSpeed: 500,
                    useCSS: false,
                    easing: 'linear'
                },
                sliderAnimationOnTouchDevice: 'fade',
                beforeFunction: function () {},
                logo: undefined
            },
            has_thumbs, thumbs, transitionDuration, firstThumb, slide, froogaloop, iframe;
        
		settings = jQuery.extend(true, settings, options);
        
        // Moves the description to the bottom of the slide
		function moveDescriptionToBottom(slide) {
			if ($(window).width() < 1024) {
				var description = slide.find('.description'),
                    descriptionHeight;
				if (description.length) {
					descriptionHeight = description.outerHeight();
					slide.css('background-size', '100% ' + (slide.height() - descriptionHeight) + 'px');
				}
			} else {
				slide.css('background-size', '');
			}
		}

		// Auto slideshow
		if (parseInt(sliderElement.data('auto-slideshow'), 10) === 1) {
			settings.sliderOptions.slideshow = true;
			settings.sliderOptions.slideshowSpeed = parseInt(sliderElement.data('slideshow-speed'), 10) * 1000;
		} else {
			settings.sliderOptions.slideshow = false;
		}

		// One slide only?
		if (sliderElement.find('.slides li').length === 1) { sliderElement.addClass('one-slide'); }

		// Keep the aspect ratio gallery
		if (sliderElement.hasClass('keep-aspect-ratio')) {
			// Make the height variable
			settings.sliderOptions.smoothHeight = true;
			// Move the description to the bottom of the slide
			moveDescriptionToBottom(sliderElement.find('.slides li').first());
			$(window).resize(function () {
				if ($(window).width() < 768) { moveDescriptionToBottom(sliderElement.find('.slides li.flex-active-slide')); }
			});
		}

		// Control the thumbnails behaviour
		has_thumbs = false;
        thumbs = 0;
		if (sliderElement.hasClass('thumb-nav')) {
			has_thumbs = true;
			thumbs = $(sliderElement.parent().find('nav.munich-gallery-thumbs li'));

			// Set the thumbs progress bar transition duration
			transitionDuration = settings.sliderOptions.slideshowSpeed / 1000;
			thumbs.find('.progress').css({
				'transition-duration' : transitionDuration + 's',
				'-webkit-transition-duration' : transitionDuration + 's',
				'-moz-transition-duration' : transitionDuration + 's',
				'-o-transition-duration' : transitionDuration + 's'
			});
			
			// Activate the first thumb
			firstThumb = thumbs.first();
			firstThumb.addClass('active');
			sliderElement.parent().find('.active-thumb').css({
				'background-image' : 'url("' + firstThumb.data('image') + '")',
				'left' : firstThumb.offset().left - thumbs.parent().parent().offset().left
			});

			if (settings.sliderOptions.slideshow) { thumbs.eq(1).addClass('next'); }
			
			// Change to the corresponding slide when a thumb is clicked, and stop the animation.
			thumbs.on(MunichTheme.options.settings.clickEventEnd, function () {
				// Move to the slide
				sliderElement.flexslider($(this).index());
				// Stop the animation
				settings.sliderOptions.slideshow = false;
				thumbs.removeClass('next');
			});
			
			// Show the thumbnails on hover.
			sliderElement.parent().find('nav.munich-gallery-thumbs').hover(function () {
                if(($(".flex-active-slide").length > 0 && $(".flex-active-slide").hasClass("playing"))){
                    $(this).parent().removeClass('thumbs-visible');
                } else {
                    $(this).parent().addClass('thumbs-visible');
                }
			}, function () {
				$(this).parent().removeClass('thumbs-visible');
			});
		}

		// Set the before function
		settings.sliderOptions.before = settings.beforeFunction;

		// Set the slider animation if on a touch device
		if (window.touch_device) {
			sliderElement.addClass('touch');
			settings.sliderOptions.animation = settings.sliderAnimationOnTouchDevice;
		} else {
			sliderElement.addClass('not-touch');
		}
        
        // Enables the distraction-free mode
		function enableDistractionFreeMode() {
			$(MunichTheme.options.elements.body).addClass(settings.distractingFreeClass);
			settings.distractingElements.addClass(settings.distractingFreeClass);
			window.mouseMoveSteps = 0;
			sliderElement.on('mousemove', function () {
				window.mouseMoveSteps += 1;
				if (window.mouseMoveSteps > 45) {
					settings.distractingElements.removeClass(settings.distractingFreeClass);
					window.mouseMoveSteps = 0;
				}
			});
            if (window.touch_device) {
                sliderElement.on(MunichTheme.options.settings.clickEventEnd, function () {
                    settings.distractingElements.removeClass(settings.distractingFreeClass);
                });
            }
		}
        
        // Prevents the logo from overlapping with the description area
		function setLogoPosition(slide) {
			var logoParent = settings.logo.parent(),
                description = slide.find('.description'),
                descriptionHeight = description.outerHeight();

			if (description.length && (settings.logo.offset().left + settings.logo.width()) - description.offset().left >= 0) {
				// Only if there is a description present and there is x axis colition
				logoParent.addClass('transitioning');
				setTimeout(function () {
					logoParent.attr('style', '');
					if ((settings.logo.offset().top + settings.logo.height()) - description.offset().top >= 0) {
						logoParent.css('bottom', descriptionHeight + parseInt(description.css('bottom'), 10));
					} else {
						logoParent.attr('style', '');
					}
					setTimeout(function () {
						logoParent.removeClass('transitioning');
					}, 250);
					
				}, 250);
				
			} else {
				if (logoParent.attr('style') && logoParent.attr('style').length > 0) {
					logoParent.addClass('transitioning');
					setTimeout(function () {
						logoParent.attr('style', '');
						setTimeout(function () {
							logoParent.removeClass('transitioning');
						}, 250);
					}, 250);
					
				}
			}
		}
        
        // Adds a "flex-prev-slide" class to the slide that we are animating from
		function setPreviusSlideClass(slider) {
			slider.animatingFrom = (slider.animatingTo === 0) ? slider.last : slider.animatingTo - 1;
			if (slider.direction === 'prev') {
				slider.animatingFrom = (slider.animatingTo === slider.last) ? 0 : slider.animatingTo + 1;
			}

			var slide = sliderElement.find('.slides li').eq(slider.animatingTo),
                prevSlide = sliderElement.find('.slides li').eq(slider.animatingFrom);
			sliderElement.find('.slides li').removeClass('flex-prev-slide');
			prevSlide.addClass('flex-prev-slide');
		}
		
		settings.sliderOptions.before = function (slider) {
			window.mouseMoveSteps = 0;
			settings.beforeFunction(slider);
			if (sliderElement.hasClass('keep-aspect-ratio')) { moveDescriptionToBottom(sliderElement.find('.slides li').eq(slider.animatingTo)); }
			if (settings.distractionFree) { enableDistractionFreeMode(); }
			
			if (settings.logo) { setLogoPosition(sliderElement.find('.slides li').eq(slider.animatingTo)); }

			setPreviusSlideClass(slider);

			// Hide the scroll animation
			setTimeout(function () {
				sliderElement.removeClass('on-first-slide');
			}, 250);
			sliderElement.find('.scroll-animation').removeClass('visible');

			// Thumbs animation
			if (has_thumbs) {
				thumbs.removeClass('active next');

				var activeThumb = thumbs.eq(slider.animatingTo);

				activeThumb.addClass('active');
				sliderElement.parent().find('.active-thumb').css('background-image', 'url("' + activeThumb.data('image') + '")');
                if (window.touch_device) {
                    sliderElement.parent().find('.active-thumb').css('left', activeThumb.position().left);
                } else {
                    sliderElement.parent().find('.active-thumb').css('left', activeThumb.offset().left - thumbs.parent().parent().offset().left);
                }

				if (settings.sliderOptions.slideshow) {
					thumbs.eq(slider.animatingTo + 1).addClass('next');
					if (thumbs.parent().find('.next').length === 0) {
						thumbs.first().addClass('next');
					}
				}
			}

			// Play video when entering the slide
			slide = sliderElement.find('.slides li').eq(slider.animatingTo);
			if (slide.find('iframe').length > 0) {
				if (slide.data('video').indexOf('vimeo') > -1) {
					froogaloop = $f('video_' + slide.index());
					froogaloop.api('play');
				} else if (slide.data('video').indexOf('youtu') > -1) {
				    iframe = $('#video_' + slide.index())[0].contentWindow;
				    iframe.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
				}
				if (slide.hasClass('playing')) {
					slide.removeClass('playing').addClass('paused');
				} else {
					slide.removeClass('paused').addClass('playing');
				}
			}
		};
        
        function pauseVideo(slide) {
			if (slide.find('iframe').length > 0) {
				if (slide.data('video').indexOf('vimeo') > -1) {
				    froogaloop = $f('video_' + slide.index());
					froogaloop.api('pause');
				} else if (slide.data('video').indexOf('youtu') > -1) {
				    iframe = $('#video_' + slide.index())[0].contentWindow;
				    iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
				}
				slide.removeClass('playing').addClass('paused');
			}
		}

		function playVideo(slide, video) {
			if (slide.find('iframe').length > 0) {
				if (slide.data('video').indexOf('vimeo') > -1) {
					froogaloop = $f('video_' + slide.index());
					froogaloop.api('pause');
				} else if (slide.data('video').indexOf('youtu') > -1) {
					iframe = $('#video_' + slide.index())[0].contentWindow;
				    iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
				}
			} else {
				createVideoFrame(video, slide);
			}
			slide.removeClass('paused').addClass('playing');
		}
        
        function toggleVideoState(slide) {
			var video_url = slide.data('video'),
                video = parseVideo(video_url);

			if (slide.hasClass('playing')) {
				pauseVideo(slide);
			} else {
				playVideo(slide, video);
			}
		}

		settings.sliderOptions.after = function (slider) {
			pauseVideo(sliderElement.find('.slides li').eq(slider.animatingFrom));
		};

		// Video
		$('.video-play').on(MunichTheme.options.settings.clickEvent, function () {
			toggleVideoState($(this).parent());
		});
		
		// Initialize the gallery
		this.flexslider(settings.sliderOptions);
		if (settings.logo) { setLogoPosition($('.flex-active-slide')); }
	};
    
    /* 
	 * Fills the space neccesasry for the element to be at the very bottom of the page.
	 *
	*/
    jQuery.fn.fillSpaceUntilBottom = function (options) {
        var element = $(this),
            settings = {
                elementWrapper: element.parent(),
                elementsOnTop: element.prev(),
                minimumWindowWidth: 1024,
                adjustOnWindowResize: true
            };
        settings = $.extend(true, settings, options);
        
        function fillSpace() {
            var elementsOnTopHeight = 0;
            settings.elementsOnTop.each(function () {
                elementsOnTopHeight += $(this).outerHeight();
            });
                        
            var innerHeight = elementsOnTopHeight + element.outerHeight(),
                desiredInnerHeight = settings.elementWrapper.height(),
                offset = desiredInnerHeight - innerHeight;
            
            if (offset > 0 && $(window).width() >= settings.minimumWindowWidth) {
                element.css('margin-top', offset);
            } else {
                element.css('margin-top', 0);
            }
        }
        
        fillSpace();
        if (settings.adjustOnWindowResize) {
            $(window).resize(fillSpace);
        }
        setTimeout(fillSpace, 500);
    };

	/* 
	 * Adds a toggle functionality to an element when clicked
	 *
	 *  - buttonsSelector:string           the elements that will trigger the toggle when clicked.
	 *  - toggleClass:string               the class to be toggled on the matching {targetSelector} elements .
	 *  - targetSelector:string            the elements whose {toggleClass} class will be toggled.
	 *  - replicateClassSelector:string	   the elements to whom {targetClass} class will be appended/removed according to the result of the toggle:
	 *                                         append when the toggle appends the class to the {targetSelector} elements
	 *                                         remove when the toggle removes the class from the {targetSelector} elements
	 *  - targetCloseSelector:string	the elements whose {toggleClass} class will be removed.
	 *  - toggleButtonClass:string		the class to be toggled for the {buttonsSelector} elements.
	 */
	jQuery.fn.toggleButton = function (options) {
		/* Default options values */
		var settings = {
			toggle: {
				elements: $(''),
				classes: 'open'
			},
			add: {
				elements: $(''),
				classes: 'open'
			},
			remove: [{ elements: $(''), classes: 'open' }],
			replicate: [{ elements: $(''), classes: 'open' }],
			whenToggleOn: function () { },
			whenToggleOff: function () { },
			unless: function () { return false; }
		},
            button,
            target,
            replicate;
		settings = $.extend(true, settings, options);

		button = this;
		target = settings.toggle.elements;
		replicate = settings.replicate;

		button.off(MunichTheme.options.settings.clickEvent);
        button.on(MunichTheme.options.settings.clickEvent, function (e) {
            e.preventDefault();
            
			if (!settings.unless()) {
				// Toggle & replicate
				if (target.hasClass(settings.toggle.classes)) {
					target.removeClass(settings.toggle.classes);
					replicate.forEach(function (v) {
						v.elements.removeClass(v.classes);
					});
					settings.whenToggleOff();
				} else {
					target.addClass(settings.toggle.classes);
					replicate.forEach(function (v) {
						v.elements.addClass(v.classes);
					});
					settings.whenToggleOn();
				}
				// Add
				settings.add.elements.addClass(settings.add.classes);
				// Remove
				settings.remove.forEach(function (v) {
					v.elements.removeClass(v.classes);
				});
			}
		});
	};

	/* 
	 * Adds a toggle functionality to an element when clicked
	 *
	 *  - buttonsSelector:string           the elements that will trigger the toggle when clicked.
	 *  - toggleClass:string               the class to be toggled on the matching {targetSelector} elements .
	 *  - targetSelector:string            the elements whose {toggleClass} class will be toggled.
	 *  - replicateClassSelector:string    the elements to whom {targetClass} class will be appended/removed according to the result of the toggle:
	 *                                         append when the toggle appends the class to the {targetSelector} elements
	 *                                         remove when the toggle removes the class from the {targetSelector} elements
	 *  - targetCloseSelector:string       the elements whose {toggleClass} class will be removed.
	 *  - toggleButtonClass:string         the class to be toggled for the {buttonsSelector} elements.
	 */
	jQuery.fn.scrollableOnHover = function (options) {
		/* Default options values */
		var settings = {
			wrapper: $(this),
			inner: $(this).find('.inner'),
			items: $(this).find('.item'),
			touchDevice: false
		},
            topRightOffset = -(settings.inner.width() - settings.wrapper.width());
		settings = $.extend(true, settings, options);

		settings.wrapper.on('mousemove', function (e) {
			var wrapperWidth = settings.wrapper.width(),
				innerWidth = settings.inner.width(),
				windowWidth = $(window).width(),
                itemWidth,
                mousePos,
                offsetPercentage,
                activeThumb;

			if (innerWidth > wrapperWidth && !settings.touchDevice) {
                itemWidth = settings.items.width();
                mousePos = e.pageX;
                offsetPercentage = ((mousePos - itemWidth) * 100) / (settings.wrapper.width() - 2 * itemWidth);

                if (offsetPercentage < 0) {
                    offsetPercentage = 0;
		        } else if (offsetPercentage > 100) {
                    offsetPercentage = 100;
		        }

		        settings.inner.css('left', topRightOffset * (offsetPercentage / 100));
                
                activeThumb = $(this).find('.active-thumb');
                if (activeThumb) {
                    activeThumb.css('left', (parseFloat(settings.wrapper.find('.active').offset().left) - parseFloat(settings.wrapper.offset().left)) + "px");
                }
                
		    } else {
                settings.inner.attr('style', '');
		    }
		});
	};

	/* 
	  Used for showing a spinner while an element's background image is still loading.

	   - elementsSelector:string       the elements whose images will be loaded.
	   - imageDataAttribute:string     the data attribute where the image URL is stored.
	   - spinnerHtml:string            the html code for the spinner
	*/
	jQuery.fn.preloadImages = function (options) {
		/* Default options values */
		var settings = {
			imageDataAttribute: 'image',
			spinnerHtml: '<div class="image-spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>',
			onFinishLoad: function () {}
		},
            elements = this;
        
		settings = $.extend(true, settings, options);

		// Append the spinner's HTML code
        $(elements).find(".image-spinner").remove();
		elements.prepend(settings.spinnerHtml);

		// Load the images
		elements.each(function () {
			var element = $(this),
                image_url = element.data(settings.imageDataAttribute);
			// Set the element's background image only when the image has loaded.
            if (image_url) {
                $('<img>').attr({ src: image_url }).load(function () {
                    element.css('background-image', 'url("' + image_url + '")').removeClass('not-done').addClass('done');
                    if (elements.filter('.done').length === elements.length) { settings.onFinishLoad(); }
                });
            } else {
                element.addClass('no-image');
            }
		});
		
	};

	$('.slides-nav.desktop .next').click(function(event) {
		var list = $('#main-slider-desktop .slides').find('.slide');
		list.each(function(index, slide){

			if ( $(slide).hasClass('flex-active-slide') ) {
				$(slide).animate({opacity: 0}, 300).removeClass('flex-active-slide');
		
				if ( index != (list.length - 1) ) {
					$(list[index + 1]).animate({opacity: 1}, 300).addClass('flex-active-slide');
				} else {
					$(list[0]).animate({opacity: 1}, 300).addClass('flex-active-slide');
				}
				return false;
			}
		});
	});

	$('.slides-nav.desktop .prev').click(function(event) {
		var list = $('#main-slider-desktop .slides').find('.slide');
		list.each(function(index, slide){

			if ( $(slide).hasClass('flex-active-slide') ) {
				$(slide).animate({opacity: 0}, 300).removeClass('flex-active-slide');
	
				if ( index != 0 ) {
					$(list[index - 1]).animate({opacity: 1}, 300).addClass('flex-active-slide');
				} else {
					$(list[list.length-1]).animate({opacity: 1}, 300).addClass('flex-active-slide');
				}
				return false;
			}
		});
	});

	$('.slides-nav.mobile .next').click(function(event) {
		var list = $('#main-slider-mobile .slides').find('.slide');
		list.each(function(index, slide){

			if ( $(slide).hasClass('flex-active-slide') ) {
				$(slide).animate({opacity: 0}, 300).removeClass('flex-active-slide');
		
				if ( index != (list.length - 1) ) {
					$(list[index + 1]).animate({opacity: 1}, 300).addClass('flex-active-slide');
				} else {
					$(list[0]).animate({opacity: 1}, 300).addClass('flex-active-slide');
				}
				return false;
			}
		});
	});

	$('.slides-nav.mobile .prev').click(function(event) {
		var list = $('#main-slider-mobile .slides').find('.slide');
		list.each(function(index, slide){

			if ( $(slide).hasClass('flex-active-slide') ) {
				$(slide).animate({opacity: 0}, 300).removeClass('flex-active-slide');
	
				if ( index != 0 ) {
					$(list[index - 1]).animate({opacity: 1}, 300).addClass('flex-active-slide');
				} else {
					$(list[list.length-1]).animate({opacity: 1}, 300).addClass('flex-active-slide');
				}
				return false;
			}
		});
	});

}(jQuery));


String.prototype.capitalize = function () {
    "use strict";
    return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
};

function createVideoFrame(video, container) {
    "use strict";
	if (video.type === "vimeo") {
		container.append('<iframe id="video_' + container.index() + '" class="vimeo-player" src="//player.vimeo.com/video/' + video.id + '?autoplay=1" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
	} else if (video.type === "youtube") {
		container.append('<iframe id="video_' + container.index() + '" class="youtube-player" src="http://www.youtube.com/embed/' + video.id + '?enablejsapi=1&api=1&player_id=video_' + container.index() + '&autoplay=1&showinfo=0&controls=0" width="100%" height="100%"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
	}
}

function parseVideo(url) {
    "use strict";
    url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
    var type;
    if (RegExp.$3.indexOf('youtu') > -1) {
        type = 'youtube';
    } else if (RegExp.$3.indexOf('vimeo') > -1) {
        type = 'vimeo';
    }
    return { type: type, id: RegExp.$6 };
}

function swapNodes(a, b) {
    "use strict";
    var aparent = a.parentNode,
        asibling = (a.nextSibling === b) ? a : a.nextSibling;
    b.parentNode.insertBefore(a, b);
    aparent.insertBefore(b, asibling);
}