<?php 
/*
Template Name: Contact
*/

get_header(); ?>
<?php if ( !post_password_required( $post ) ) {  ?>
<div id="page-body" class="contact custom-page">
<?php 
    while ( have_posts() ) : the_post(); ?>
    <?php $featured_image = wp_get_attachment_url( get_post_thumbnail_id() );
          $show_map = false;
          $api_key = esc_attr( redux_post_meta( "theme_options", $post->ID, "contact_page_map_apy_key" ) );
          $lat = floatval( redux_post_meta( "theme_options", $post->ID, "contact_page_map_lat" ) );
          $lng = floatval( redux_post_meta( "theme_options", $post->ID, "contact_page_map_lng" ) );
          $zoom = intval( redux_post_meta( "theme_options", $post->ID, "contact_page_map_zoom" ) );
          if( strlen( $lat ) > 0 && strlen( $lng ) > 0 ) $show_map = true;
          $show_title = redux_post_meta( "theme_options", $post->ID, "custom-page-show-title" );
          $show_title_shadow = redux_post_meta( "theme_options", $post->ID, "custom-page-show-title-shadow" );
          if($show_title) { $show_title = true; } else { $show_title = false; }
          if($show_title_shadow) { $show_title_shadow = "show-title-shadow"; } else { $show_title_shadow = ""; }
    ?>
    <section class="image desktop preload <?php echo esc_attr( $show_title_shadow ); ?>" data-image="<?php echo esc_attr( $featured_image ); ?>">
        <div class="social">
            <?php get_template_part( 'partials/modules/social_networks_links' ); ?>
        </div>
    <?php if( $show_map ){ ?>
        <a id="toggle-map-desktop" class="toggle-map icon icon-marker-1" href="#"></a>
        <div id="contact-map-desktop" class="contact-map">
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_attr( $api_key ); ?>">
            </script>
            <script type="text/javascript">
              function initialize() {
                var mapOptions = {
                  center: { lat: <?php echo esc_attr( $lat ); ?>, lng: <?php echo esc_attr( $lng ); ?>},
                  zoom: <?php echo esc_attr( $zoom ); ?>
                };
                var mapDesktop = new google.maps.Map(document.getElementById('contact-map-canvas-desktop'), mapOptions);
                var mapMobile = new google.maps.Map(document.getElementById('contact-map-canvas-mobile'), mapOptions);
                
                var markerDesktop = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo esc_attr( $lat ); ?>, <?php echo esc_attr( $lng ); ?>),
                    map: mapDesktop
                });
                var markerMobile = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo esc_attr( $lat ); ?>, <?php echo esc_attr( $lng ); ?>),
                    map: mapMobile
                });
              }
              google.maps.event.addDomListener(window, 'load', initialize);
            </script>
            <div id="contact-map-canvas-desktop"></div>
        </div>
    <?php } ?>
    </section>
    <section class="content">
        <div class="inner">
            <?php echo the_content(); ?>
        </div>
        <section class="image mobile preload" data-image="<?php echo esc_attr( $featured_image ); ?>">
            <div class="social">
                <?php get_template_part( 'partials/modules/social_networks_links' ); ?>
            </div>
            <?php if( $show_map ){ ?>
            <a id="toggle-map-mobile" class="toggle-map icon icon-marker-1" href="#"></a>
            <div id="contact-map-mobile" class="contact-map">
                <div id="contact-map-canvas-mobile"></div>
            </div>
            <?php } ?>
        </section>
        <section class="contact-form">
            <?php echo do_shortcode( redux_post_meta( "theme_options", $post->ID, "contact_page_form_shortcode" ) ); ?>
        </section>
    </section>
    <?php endwhile; ?>
</div>
<?php
} else { 
    echo get_the_password_form();
} ?>
<?php get_footer(); ?>