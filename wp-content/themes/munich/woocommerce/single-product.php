<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>

<div id="page-body" class="single-product">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php wc_get_template_part( 'content', 'single-product' ); ?>

	<?php endwhile; // end of the loop. ?>
</div>
<button id="toggle-sidebar" class="toggle-button">
	<span class="bar first"></span>
	<span class="bar middle"></span>
	<span class="bar last"></span>
</button>
<section class="sidebar shop-sidebar">
	<div class="wrapper">
		<?php get_sidebar( 'shop' ); ?>
	</div>
</section>

<?php get_footer( 'shop' ); ?>