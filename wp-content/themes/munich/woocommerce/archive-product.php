<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); 

if ( !isset( $post ) || !post_password_required( $post ) ) { ?>

<div id="page-body" class="shop shop-archive posts-gallery sidebar-<?php //echo esc_attr( $portfolio_options->sidebar_visibility ) AND munich_shop_show_overlays; ?>">
	<div class="flexslider">
		<section id="shop-list" class="slides">
			<div class="image-spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		// do_action( 'woocommerce_before_main_content' );
	?>
		<?php if ( have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<!-- NO PRODUCTS -->

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		// do_action( 'woocommerce_after_main_content' );

		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>
		</section>
	</div>

	<section class="sidebar">
		<div class="wrapper">
			<?php get_sidebar( 'shop' ); ?>
		</div>
		<div class="pagination">
			<span id="current-page" data-current-page="1" data-pages="">
				<span>1</span><span>/</span><span></span>
			</span>
			<button id="prev-page" class="icon icon-arrow-65"></button>
			<button id="next-page" class="icon icon-arrow-66"></button>
		</div>
	</section>

</div>
<?php
} else {
	echo get_the_password_form();
}	
?>

<?php get_footer( 'shop' ); ?>