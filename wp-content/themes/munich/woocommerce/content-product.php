<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
$classes[] = 'item shop-item';

$block_size = "portrait";
if( get_post_meta( $post->ID, 'munich_product_block_type', true ) == "two" ) {
    $block_size = "wide";
}
$classes[] = $block_size;

$thumb_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $block_size . '_thumb' );
?>
<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
    <div class="overlay"></div>
	<div class="image preload" data-image="<?php echo esc_attr( $thumb_src[0] ); ?>"></div>
	<div class="description product-description">
		<a href="<?php the_permalink(); ?>">
			<h5><?php the_title(); ?></h5>
		</a>
		<div class="price-wrapper">
			<div class="price-inner">
		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
		?>
		<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
			</div>
		</div>
	</div>

	<?php
	if( function_exists( 'woocommerce_show_product_loop_sale_flash' ) )
			woocommerce_show_product_loop_sale_flash();
	?>
</li>