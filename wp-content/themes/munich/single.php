<?php get_header(); ?>

<?php if ( !post_password_required( $post ) ) {  ?>

<?php 
	
	global $post;
	setup_postdata( $post ); 

	// Gallery options
	$prefix = "munich_blog";
	$has_logo = false;
	$keep_aspect_ratio = false;
	$is_blog = true;
	$posts_navigation = true;

	$meta_info = (object) array(
		'title'		=> 	get_the_title(),
		'date'		=> 	get_the_date(),
		'author'	=>	get_the_author()
    );
?>
<div id="page-body" class="single-post">

	<?php include(locate_template( 'partials/modules/gallery.php' )); ?>

	<article class="post">
		<div class="inner">
			<div class="content">
				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
				
                <footer>
                    <p class="tags"><?php the_tags(); ?></p>
                    <div class="pages"><?php wp_link_pages(); ?></div>
                </footer>
			</div>
			<?php comments_template(); ?>
		</div>
	</article>
</div>
<button id="toggle-sidebar" class="toggle-button">
	<span class="bar first"></span>
	<span class="bar middle"></span>
	<span class="bar last"></span>
</button>
<section class="sidebar blog-sidebar">
	<div class="wrapper">
		<?php 
			$cats = get_the_category();
			$id = $cats[0]->cat_ID;
			
			switch ($id) {
				case 12:
					get_sidebar();
					break;
				
				default:
					get_sidebar('shop');
					break;
			}
		?>
		
	</div>
</section>

<?php
} else {
	echo get_the_password_form();
}	
?>

<?php get_footer(); ?>