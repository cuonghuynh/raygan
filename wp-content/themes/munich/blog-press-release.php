<?php 
/*
Template Name: Press Release
*/
get_header();
if ( !post_password_required( $post ) ) { 
	include(locate_template('partials/loops/blog-press-release-loop.php'));
} else {
	echo get_the_password_form();
}

get_footer();
?>