<?php 
get_header();

$cat = get_query_var('cat');

switch ($cat) {
	case 12:
		if ( !post_password_required( $post ) ) { 
			include(locate_template('partials/loops/blog-loop.php'));
		} else {
			echo get_the_password_form();
		}
		break;
	
	default:
		if ( !post_password_required( $post ) ) { 
			include(locate_template('partials/loops/blog-press-release-loop.php'));
		} else {
			echo get_the_password_form();
		}
		break;
}


get_footer(); 
?>