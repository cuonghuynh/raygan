<?php
/*
Template Name: Homepage
*/

get_header();

if ( !post_password_required( $post ) ) { ?>
<?php if( function_exists( 'redux_post_meta') ) { ?>
    <div id="page-body" class="homepage">
        <?php 
        // Gallery options
        $prefix = "homepage";
        $has_logo = true; 
        $keep_aspect_ratio = false;
        // Call gallery
        include(locate_template( 'partials/modules/gallery.php' )); 
        ?>
    </div>
<?php }
} else {
	echo get_the_password_form();
}
get_footer(); ?>