<?php 

function munich_shape_comment( $comment, $args, $depth ) {
  $GLOBALS['comment'] = $comment;
  switch ( $comment->comment_type ) :
      case 'pingback' :
      case 'trackback' :
  ?>
  <li class="post pingback">
      <p><?php _e( 'Pingback:', 'munich' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'munich' ), ' ' ); ?></p>
  <?php
          break;
      default :
  ?>
  <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
      <article id="comment-<?php comment_ID(); ?>" class="comment">
      	
      	<div class="comment-wrap">
      		<?php echo get_avatar( $comment, 55 ); ?>
					<div class="content">
						<p class="author-name"><strong><?php echo get_comment_author_link(); ?>&nbsp;</strong></p><?php comment_text(); ?>
						<?php if ( $comment->comment_approved == '0' ) : ?><em><?php _e( 'Your comment is awaiting moderation.', 'munich' ); ?></em><?php endif; ?>
						<footer>
							<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
								<time pubdate datetime="<?php comment_time( 'c' ); ?>"><?php printf( __( '%1$s at %2$s', 'munich' ), get_comment_date(), get_comment_time() ); ?> </time>
							</a>
	            <?php edit_comment_link( __( '(Edit)', 'munich' ), ' ' ); ?>
	            <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
						</footer>
					</div>
				</div>
      </article><!-- #comment-## -->

  <?php
          break;
  endswitch;
}

function munich_get_vc_custom_shortcode_styles() {
    $mypages = get_pages();
    foreach( $mypages as $page ) {
        $shortcodes_custom_css = get_post_meta( $page->ID, '_wpb_shortcodes_custom_css', true );
        if ( ! empty( $shortcodes_custom_css ) ) {
            echo '<style type="text/css" data-type="vc_shortcodes-custom-css">';
            echo get_post_meta( $page->ID, '_wpb_shortcodes_custom_css', true );
            echo '</style>';
        }
    }
}

function remove_querystring_var($url, $key) { 
	list($file, $parameters) = explode('?', $url);
	parse_str($parameters, $output);
	unset($output[$key]);
	$result = $file . '?' . http_build_query($output); // Rebuild the url
	return $result;
}

function wp_get_attachment_medium_url( $id )
{
    $medium_array = image_downsize( $id, 'medium' );
    $medium_path = $medium_array[0];

    return $medium_path;
}

function hex2rgbopacity($hex, $opacity) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return "rgba(" . implode(",", $rgb) . "," . $opacity. ")";
}

?>