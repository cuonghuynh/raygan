<?php
function munich_get_custom_styles() {
    global $theme_options;
    $primary = $theme_options['site-primary-color'];
    $secondary = $theme_options['site-secondary-color'];
    $sidebar_text = $theme_options['sidebar-text-color'];
?>
    <style type="text/css">
        /* Primary color */
        <?php if(isset($primary)){ ?>
        #site-header > .bar, #main-nav, .sidebar, .munich-gallery.thumb-nav, .posts-gallery li.item span.price, .single-portfolio > nav, .post .date, #toggle-sidebar, section.comments #respond, #cart-qty .amount, #cart-content { background: <?php echo esc_attr( $primary ); ?>; }
        .vc_progress_bar .vc_single_bar.bar_red .vc_bar { background-color: <?php echo esc_attr( $secondary ); ?>; }
        
        .wpb_call_to_action { background: <?php echo hex2rgbopacity($secondary, "0.1"); ?>!important; }
        <?php } ?>
        /* Secondary color */
        <?php if(isset($secondary)){ ?>
        .posts-gallery .load-more, #toggle-main-nav, .munich-gallery .description h6 em, .sidebar .cart, .post .date .day:after, .posts-gallery li.item .price-inner a, .wpb_call_to_action .wpb_button, input[type="submit"], #respond input[type="submit"], #submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit, .woocommerce #content input.button, .woocommerce-page a.button, .woocommerce-page button.button, .woocommerce-page input.button, .woocommerce-page #respond input#submit, .woocommerce-page #content input.button, .woocommerce-page #respond input#submit, .woocommerce-page div.product form.cart .button, .post .date:before { background: <?php echo esc_attr( $secondary ); ?>; }
        .posts-gallery li.item .price-inner a:hover { background-color: <?php echo esc_attr( hex2rgbopacity($secondary, "1.0") ); ?>; }
        .munich-gallery-thumbs .active-thumb { border-color: <?php echo esc_attr( $secondary ); ?>; }
        .posts-gallery li.item .description span, .widget_search label:before, .woocommerce .price .amount { color: <?php echo esc_attr( $secondary ); ?>; }
        <?php } ?>
        /* Sidebar text color */
        <?php if(isset($sidebar_text)){ ?>
        #main-nav .menu li:hover > a, #main-nav p, #main-nav .nav-text, #featured-projects .content a, .sidebar ul li a, .portfolio .sidebar ul li a, .shop .sidebar ul li a, ul#cart-items li span.cart-product-quantity, ul#cart-items li span.cart-product-quantity *, #cart-total .amount {  color: <?php echo esc_attr( $sidebar_text ); ?>; }
        <?php } ?>
        
        @media all and (min-width: 768px) {
            <?php if(isset($primary)){ ?>
            #page-body.shop .sidebar { background: <?php echo esc_attr( $primary ); ?>; }
            <?php } ?>
            <?php if(isset($primary)){ ?>
            .blog-posts .post .excerpt:before { background: <?php echo esc_attr( $secondary ); ?>; }
            <?php } ?>
        }
        
        @media all and (min-width: 1024px) {
            <?php if(isset($primary)){ ?>
            /* Primary color */
            #featured-projects .image:before, .sidebar .pagination, .blog-posts .post a.read-more { background: <?php echo esc_attr( $primary ); ?>; }
            <?php 
                $primary_zero_opacity = hex2rgbopacity($primary, "0");
                $primary_sixty_opacity = hex2rgbopacity($primary, "0.6");
                $primary_eightyfive_opacity = hex2rgbopacity($primary, "0.85"); 
                $primary_full_opacity = hex2rgbopacity($primary, "1");
            ?>
            .sidebar{
                background: -webkit-linear-gradient(left, <?php echo esc_attr( $primary_sixty_opacity ); ?> 0%, <?php echo esc_attr( $primary_full_opacity ); ?> 90%);
                background: linear-gradient(to right, <?php echo esc_attr( $primary_sixty_opacity ); ?> 0%, <?php echo esc_attr( $primary_full_opacity ); ?> 90%);

            }
            .sidebar .wrapper:before {
                background: -webkit-linear-gradient(top, <?php echo esc_attr( $primary_zero_opacity ); ?> 0%, <?php echo esc_attr( $primary_full_opacity ); ?> 18%, <?php echo esc_attr( $primary_full_opacity ); ?> 37%, <?php echo esc_attr( $primary_full_opacity ); ?> 100%);
                background: linear-gradient(to bottom, <?php echo esc_attr( $primary_zero_opacity ); ?> 0%,<?php echo esc_attr( $primary_full_opacity ); ?> 18%,<?php echo esc_attr( $primary_full_opacity ); ?> 37%,<?php echo esc_attr( $primary_full_opacity ); ?> 100%);
            }
            
            /*body.blog .sidebar, body.search .sidebar {
                background: -webkit-linear-gradient(left, <?php echo esc_attr( $primary_eightyfive_opacity ); ?> 0%, <?php echo esc_attr( $primary_full_opacity ); ?> 90%);
                background: linear-gradient(to right, <?php echo esc_attr( $primary_eightyfive_opacity ); ?> 0%, <?php echo esc_attr( $primary_full_opacity ); ?> 90%);

            }*/

            <?php } ?>
            /* Secondary color */
            <?php if(isset($secondary)){ ?>
            .sidebar .pagination, .sidebar .title .text span:before { color: <?php echo esc_attr( $secondary ); ?>; }
            body.home #site-header button#toggle-featured-projects, #toggle-work-bottom-bar, .sidebar.blog-sidebar .title .text:after { background: <?php echo esc_attr( $secondary ); ?>; }
            <?php } ?>
            /* Sidebar text color */
            <?php if(isset($sidebar_text)){ ?>
            .sidebar .title em, .blog-posts .post a.read-more:hover {  color: <?php echo esc_attr( $sidebar_text ); ?>; }
            <?php } ?>
        }
        
        #page-body.error404 {
<?php   if(isset($theme_options['munich-404-background']['background-color'])){ ?>
            background-color: <?php echo esc_attr( $theme_options['munich-404-background']['background-color'] ); ?>;
<?php   } 
        if(isset($theme_options['munich-404-background']['background-image'])){ ?>
            background-image: url(<?php echo esc_attr( $theme_options['munich-404-background']['background-image'] ); ?>);
<?php   }
        if(isset($theme_options['munich-404-background']['background-repeat'])){ ?>
            background-repeat: <?php echo esc_attr( $theme_options['munich-404-background']['background-repeat'] ); ?>;
<?php   }
        if(isset($theme_options['munich-404-background']['background-position'])){ ?>
            background-position: <?php echo esc_attr( $theme_options['munich-404-background']['background-position'] ); ?>;
<?php   }
        if(isset($theme_options['munich-404-background']['background-size'])){ ?>
            background-size: <?php echo esc_attr( $theme_options['munich-404-background']['background-size'] ); ?>;
<?php   } 
        if(isset($theme_options['munich-404-background']['background-attachment'])){ ?>
            background-attachment: <?php echo esc_attr( $theme_options['munich-404-background']['background-attachment'] ); ?>;
<?php   } ?>
        }
        <?php if(isset($theme_options['munich-404-opacity'])){ ?>
        .error-404 h1 { opacity: <?php echo esc_attr( $theme_options['munich-404-opacity'] ); ?>; }
        <?php } ?>
        
        <?php 
        /* Munich Custom CSS */
        if(isset($theme_options['munich-custom-css'])) { echo balanceTags($theme_options['munich-custom-css']); } ?>
        
    </style>
<?php
}

function munich_get_custom_js() {
    /* Munich Custom JS */
    global $theme_options;
    if(isset($theme_options['munich-custom-javascript'])) { echo balanceTags($theme_options['munich-custom-javascript']); }
}