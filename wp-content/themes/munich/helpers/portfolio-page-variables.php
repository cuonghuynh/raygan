<?php


global $theme_options;
$page_ID = $post->ID;
$is_archive = false;
if( is_post_type_archive('portfolio') || is_tax() ) $is_archive = true;

if( $is_archive ) {
	$portfolio_options = (object) array(
        'theme'	                => $theme_options['munich_portfolio_archive_theme'],
		'sidebar_visibility'	=> $theme_options['munich_portfolio_archive_sidebar_visibility'],
        'show_overlays'     	=> $theme_options['munich_portfolio_archive_show_overlays'],
		'header_type'			=> $theme_options['munich_portfolio_archive_sidebar_header'],
		'header_icon'			=> $theme_options['munich_portfolio_archive_sidebar_header_icon'],
		'header_title'			=> $theme_options['munich_portfolio_archive_sidebar_header_title'],
		'header_subtitle'		=> $theme_options['munich_portfolio_archive_sidebar_header_subtitle'],
		'header_image'			=> $theme_options['munich_portfolio_archive_sidebar_header_image']
    );
}else{
	$portfolio_options = (object) array(
		'theme'	                => redux_post_meta( "theme_options", $page_ID, "munich_portfolio_theme" ),
        'sidebar_visibility'	=> redux_post_meta( "theme_options", $page_ID, "munich_portfolio_sidebar_visibility" ),
        'show_overlays'	        => redux_post_meta( "theme_options", $page_ID, "munich_portfolio_show_overlays" ),
		'header_type'			=> redux_post_meta( "theme_options", $page_ID, "munich_portfolio_sidebar_header" ),
		'header_icon'			=> redux_post_meta( "theme_options", $page_ID, "munich_portfolio_sidebar_header_icon" ),
		'header_title'			=> redux_post_meta( "theme_options", $page_ID, "munich_portfolio_sidebar_header_title" ),
		'header_subtitle'		=> redux_post_meta( "theme_options", $page_ID, "munich_portfolio_sidebar_header_subtitle" ),
		'header_image'			=> redux_post_meta( "theme_options", $page_ID, "munich_portfolio_sidebar_header_image" )
    );
}

/*
 * Get the work_types that the current portfolio page supports.
 */

$supported_terms = array();
$all_work_types = get_terms( 'work_type' );

if( !$is_archive ) {
    $portfolio_page_work_types = redux_post_meta( "theme_options", $page_id, "munich_portfolio_page_work_types" );
    if ( is_array($portfolio_page_work_types) ){
        $i = 0;
        foreach($portfolio_page_work_types as $work_type) {
            if($work_type == "1") $supported_terms[] = $all_work_types[$i];
            $i++;
        }
    }
} else {
    $portfolio_page_work_types = $all_work_types;
}
if ( sizeof( $supported_terms ) == 0 ) $supported_terms = $all_work_types;

/*
 * Get the current page's query.
 */
$query_terms = array();
if ( isset( $_GET["type"] ) && !empty( $_GET["type"] ) ) {
    $query_terms = array( $_GET["type"] );
} else {
    if ( is_array($portfolio_page_work_types) && in_array("1", $portfolio_page_work_types) ) {
        foreach($supported_terms as $term) $query_terms[] = $term->slug;	
    }
}

/*
 * The $work_type_url variable is the URL to the portfolio page, prepared to add the filters parameters correctly (default or pretty links)
 */
$current_url = $_SERVER["REQUEST_URI"];
if( strpos($current_url, "?") ) {
    // Default URL links
    $url = remove_querystring_var($current_url, 'type');
    $work_type_url = $url . '&type=';
} else {
    // Pretty links
    $work_type_url = strtok($current_url,'?') . '?type=';
}

?>