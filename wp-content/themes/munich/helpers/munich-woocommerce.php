<?php
// Remove related products from product page
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

// Register custom tab
function woo_custom_product_tab( $tabs ) {
   
    $custom_tab = array( 
      		'custom_tab' =>  array( 
    							'title' => "", 
    							'priority' => 12, 
    							'callback' => 'woo_custom_product_tab_content' 
    						)
    				);

    return array_merge( $custom_tab, $tabs );
}

// Place content in custom tab (related products in this sample)
function woo_custom_product_tab_content() {
	woocommerce_related_products();
}
add_filter( 'woocommerce_product_tabs', 'woo_custom_product_tab' );


// Custom Fields

// Display Fields
function woo_add_custom_general_fields() {
  global $woocommerce, $post;
  echo '<div class="options_group">';
  woocommerce_wp_select( 
      array( 
        'id'      => 'munich_product_block_type', 
        'label'   => __( 'Block Type', 'munich' ), 
        'options' => array(
            'one'   => __( 'Portrait', 'munich' ),
            'two'   => __( 'Landscape', 'munich' )
            )
        )
  );
  echo '</div>';
}
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );

// Save Fields
function woo_add_custom_general_fields_save( $post_id ){
	$woocommerce_select = $_POST['munich_product_block_type'];
	if( !empty( $woocommerce_select ) )
		update_post_meta( $post_id, 'munich_product_block_type', esc_attr( $woocommerce_select ) );
	
}
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );



// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
    global $munich_allowed_html_tags;
    $items = $woocommerce->cart->get_cart();
    $cart_empty = ( sizeof( $items ) == 0 );
	ob_start();
	?>
    <div id="cart-inner">
        <div id="cart-toggle-wrapper" class="noselect <?php if($cart_empty) echo "empty-cart"; ?>">
            <span id="cart-toggle"><?php _e("SHOPPING CART", "munich") ?><span class="icon icon-arrow-1-down"></span></span>
            <span id="cart-qty" class="icon icon-shopping-bag-3"><span class="amount"><?php echo esc_html( $woocommerce->cart->cart_contents_count ); ?></span></span>
        </div>
        <div id="cart-content">
            <div class="wrapper">
                <div class="inner">
                    <div id="cart-links">
                        <a href="<?php echo esc_attr( $woocommerce->cart->get_cart_url() ); ?>"><?php _e('VIEW CART', 'munich'); ?></a>
                        <a href="<?php echo esc_attr( $woocommerce->cart->get_checkout_url() ); ?>" class="<?php if ( sizeof( $woocommerce->cart->cart_contents) == 0 ) echo "disabled"; ?>"><?php _e('CHECKOUT', 'munich'); ?></a>
                    </div>
                    <?php if( !$cart_empty ) { ?>
                    <ul id="cart-items">
                        <?php foreach($items as $item => $values) { ?>
                        <li>
                            <a href="<?php echo get_post_permalink( $values['product_id'] ) ?>">
                                <?php 
                                    $product_id = $values['product_id'];
                                    $product = new WC_Product( $product_id );
                                    $product_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'thumbnail'); 
                                ?>
                                <img class="cart-product-thumbnail" src="<?php echo esc_attr( $product_thumbnail[0] ); ?>" />
                                <span class="cart-product-title"><?php echo wp_kses( $values['data']->post->post_title, $munich_allowed_html_tags ); ?></span>
                                <span class="cart-product-quantity"><?php echo esc_html( $values['quantity'] ); ?> x <?php echo wp_kses( $product->get_price_html(), $munich_allowed_html_tags ); ?></span>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                    <span id="cart-total"><?php _e("TOTAL: ", "munich"); echo wp_kses( $woocommerce->cart->get_cart_total(), $munich_allowed_html_tags ); ?></span>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
	<?php
	$fragments['#cart-inner'] = ob_get_clean();
	return $fragments;
}