<?php include( get_template_directory() . "/helpers/portfolio-page-variables.php" ); ?>

<div class="title">
    <?php 
    if ( $portfolio_options->header_type == 'text' ) {
    ?>
    <div class="text">
        <img src="<?php echo esc_attr( $portfolio_options->header_image['url'] ); ?>" />
        <h4><?php echo esc_html( $portfolio_options->header_title ); ?></h4>
        <em><?php echo esc_html( $portfolio_options->header_subtitle ); ?></em>
    </div>
    <?php } elseif (isset($portfolio_options->header_image)) { ?>
    <div class="image">
        <img src="<?php echo esc_attr( $portfolio_options->header_image['url'] ); ?>" />
    </div>
    <?php } ?>
</div>
<ul class="categories">
<?php
    $all_url = "";
    if ( $is_archive ) {
        $all_url = get_post_type_archive_link( 'portfolio' );
    }else{
        $all_url = remove_querystring_var($work_type_url, 'type');
    }
?>
    <li class="<?php if ( sizeof( $query_terms ) != 1 && sizeof( $supported_terms ) != 1 ) echo "current-term"; ?>"><a href="<?php echo esc_attr( $all_url ); ?>" class="filter_all"><?php _e('All Gallery', 'munich'); ?></a></li>
<?php
    foreach($supported_terms as $work_type){
        $term_url = $work_type_url . $work_type->slug;
        if ( $is_archive ) $term_url = get_term_link( $work_type, 'work_type' );
        $is_current_term = "";
        if ( sizeof( $query_terms ) == 1 && $query_terms[0] == $work_type->slug) $is_current_term = "current-term";
?><!--
        --><li class="<?php echo esc_attr( $is_current_term ); ?>"><a href="<?php echo esc_attr( $term_url ); ?>" class="filter_<?php echo esc_attr( $work_type->slug ); ?>"><?php echo esc_html( $work_type->name ); ?></a></li><!--
--><?php } ?>
</ul>

