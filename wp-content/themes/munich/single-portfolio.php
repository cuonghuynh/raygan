<?php get_header(); ?>

<?php if ( !post_password_required( $post ) ) {  ?>

<?php 	if( function_exists( 'redux_post_meta') ) { ?>

<div id="page-body" class="single-portfolio">

<?php if(have_posts()): while(have_posts()): the_post(); ?>

	<?php 
	// Gallery options
	$prefix = "munich_portfolio"; 
	$has_logo = false; 
	$keep_aspect_ratio = true;

	// Related Works
	$relatedWorks = redux_post_meta( "theme_options", $post->ID, "munich_portfolio_item_related_works" ); 
	$relatedWorksCnt = 0;
	if(is_array($relatedWorks) && sizeof($relatedWorks) > 0 ){ 
		foreach($relatedWorks as $workid => $value){ if(intval($value) == 1) $relatedWorksCnt++; }
	}
	$hasRelatedWorks = $relatedWorksCnt > 0;

	// Meta Information
	$metaInfo = redux_post_meta( "theme_options", $post->ID, "munich_portfolio_meta" ); 
	$metaInfoCount = 0;
	if(is_array($metaInfo) && sizeof($metaInfo) > 0 ){ 
		foreach( $metaInfo as $meta ) {  if( strlen( $meta ) > 0 ) $metaInfoCount++; }
	}

	// Content
	$content = get_the_content(); 
	$hasDescription = strlen($content) > 0;
                                                   
    // Find Portfolio List page
    $portfolio_page_url = get_site_url() . '/portfolio' ;
    if( isset( $_POST['portfolio-page-url']  ) ) {
        $portfolio_page_url = $_POST['portfolio-page-url'];
    }

		// Call gallery, <span class="slider-wrapper"> in file
		include(locate_template( 'partials/modules/gallery.php' )); 
		?>

		<nav>
			<?php next_post_link('<div class="previous icon icon-arrow-65">%link</div>', ''); ?>
			<div class="list icon icon-thumbnails"><a href="<?php echo esc_url($portfolio_page_url); ?>"></a></div>
			<?php previous_post_link('<div class="next icon icon-arrow-66">%link</div>', ''); ?>
		</nav>

	<section id="work-bottom-bar">
		<section id="work-info" class="<?php if( !$hasDescription && !$hasRelatedWorks ) echo "no-info"; ?>">
			<h1><?php echo get_the_title(); ?></h1>
			<?php if( $metaInfoCount > 0 ) { ?>
			<hr>
			<ul class="meta">
				<?php 
                global $munich_allowed_html_tags;
				foreach( $metaInfo as $key => $meta ) {
					if( strlen( $meta ) > 0 ) {
						$str = explode( ":", $meta );
						if( sizeof( $str ) > 1 ) {
							$meta = '<strong>' . array_shift( $str ) . ': </strong>';
							$meta .= esc_html( join( $str ) );
						}
						$icon = redux_post_meta( "theme_options", $post->ID, "munich_portfolio_meta_icon_{$key}" );
				 ?>
				<li class="icon icon-<?php echo esc_attr( $icon ); ?>"><?php echo wp_kses( $meta, $munich_allowed_html_tags ); ?></li>
				<?php 
					}
				} ?>
			</ul>
			<?php } ?>
			<hr>
			<?php if( $hasDescription || $hasRelatedWorks ){ ?>
			<button id="toggle-work-bottom-bar" class="icon icon-add-1"></button>
			<?php } ?>
		</section>
		<?php if( $hasDescription ){ ?>
		<div id="work-description">
			<?php the_content(); ?>
		</div>
		<?php } ?>
	<?php if( $hasRelatedWorks ) { ?>
		<section id="related-works" class="disable-select">
			<nav>
				<h1><?php _e('related', 'munich'); ?><br><strong><?php _e('WORKS', 'munich'); ?></strong></h1>
				<button class="previous icon icon-arrow-65"></button>
				<button class="next icon icon-arrow-66"></button>
			</nav>
			<div class="wrapper">
				<ul class="inner">
				<?php foreach($relatedWorks as $workid => $value){ 
						if(intval($value) == 1) { ?>
					<li><a href="<?php echo get_permalink( $workid );  ?>"><img src="<?php echo wp_get_attachment_medium_url( get_post_thumbnail_id( $workid ) ); ?>" /></a></li>
				<?php   }
					  } ?>
				</ul>
			</div>
		</section>
	<?php } ?>
	</section>
<?php endwhile; endif; ?>
</div>
<?php } ?>

<?php
} else {
	echo get_the_password_form();
}	
?>

<?php get_footer(); ?>