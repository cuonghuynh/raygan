<?php
/*
Template Name: Gallery Portfolio
*/

get_header();

if ( !post_password_required( $post ) ) { 
	include(locate_template('partials/loops/portfolio-loop.php'));
} else {
	echo get_the_password_form();
}	

get_footer(); 
?>