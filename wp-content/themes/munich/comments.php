<?php

  if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
    die ('Please do not load this page directly. Thanks!');

  if ( post_password_required() ) { ?>
    <p class="nocomments"><?php _e('This post is password protected. Enter the password to view the comments.', 'munich') ?></p>
  <?php
    return;
  }

/*-----------------------------------------------------------------------------------*/
/*  Display the comments + Pings
/*-----------------------------------------------------------------------------------*/
?>
<section class="comments <?php if(!have_comments()){ echo "no-comments"; } ?>">
    <div id="toggle-comments-wrapper"><h2 id="toggle-comments" class="open"><?php _e('COMMENTS', 'munich'); ?></h2></div>
<?php
    /*
     * If the current post is protected by a password and
     * the visitor has not yet entered the password we will
     * return early without loading the comments.
     */
    if ( post_password_required() )
        return;

    if ( have_comments() ) : ?>
        <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through? If so, show navigation ?>
        <nav role="navigation" id="comment-nav-above" class="site-navigation comment-navigation">
            <h1 class="assistive-text"><?php _e( 'Comment navigation', 'munich' ); ?></h1>
            <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'munich' ) ); ?></div>
            <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'munich' ) ); ?></div>
        </nav><!-- #comment-nav-before .site-navigation .comment-navigation -->
        <?php endif; // check for comment navigation ?>
 
        <ol class="commentlist">
          <?php wp_list_comments( array( 'callback' => 'munich_shape_comment' ) ); ?>
        </ol><!-- .commentlist -->
 
        <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through? If so, show navigation ?>
        <nav role="navigation" id="comment-nav-below" class="site-navigation comment-navigation">
            <h1 class="assistive-text"><?php _e( 'Comment navigation', 'munich' ); ?></h1>
            <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'munich' ) ); ?></div>
            <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'munich' ) ); ?></div>
        </nav><!-- #comment-nav-below .site-navigation .comment-navigation -->
        <?php endif; // check for comment navigation ?>
 
    <?php else : ?>

      <p class="no-comments"><?php _e('No comments yet.', 'munich') ?></p>

    <?php endif; // have_comments() ?>
 
    <?php
        // If comments are closed and there are comments, let's leave a little note, shall we?
        if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
    ?>
        <p class="nocomments"><?php _e( 'Comments are closed.', 'munich' ); ?></p>

    <?php endif; ?>

<?php
/*-----------------------------------------------------------------------------------*/
/*  Comment Form
/*-----------------------------------------------------------------------------------*/

  if ( comments_open() ) : ?>

  <div class="respond">

  <?php
  $commenter = wp_get_current_commenter();
  $req = get_option( 'require_name_email' );
  $aria_req = ( $req ? " aria-required='true'" : '' );
  $fields = array(
    'comment_field' => '<p class="form-comment"><textarea placeholder="' . __( 'Your comments...', 'munich' ) . '" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
    'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'munich' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
    'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out &raquo;</a>', 'munich' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
    'comment_notes_before' => '',
    'comment_notes_after' => '',
    'title_reply' => '',
    'title_reply_to' => __('Leave a Reply to %s', 'munich'),
    'cancel_reply_link' => __('x', 'munich'),
    'label_submit' => __('SUBMIT COMMENT', 'munich'),
    'fields' => apply_filters( 'comment_form_default_fields', array(
    'author' => '<p class="form-author field"><input required placeholder="'.__('Name', 'munich').'" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" /></p>',
    'email' => '<p class="form-email field"><input required placeholder="'.__('Email', 'munich').'" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" /></p>',
    ) )
  );

      comment_form($fields); ?>
  </div>
  <?php endif; ?>
</section>